package com.freelancewriter.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancewriter.R;
import com.freelancewriter.model.GeneralModel;
import com.freelancewriter.model.OrderByIdModel;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.order.OrderDetailsActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.edittext.EditTextSFTextRegular;
import com.freelancewriter.util.textview.AutoCompleteText;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderApplyFragment extends BaseFragment {

    @BindView(R.id.et_comments)
    EditTextSFTextRegular etComments;
    @BindView(R.id.tv_rating)
    AutoCompleteText tvRating;
    @BindView(R.id.ll_apply_view)
    LinearLayout llApplyView;
    @BindView(R.id.ll_cancel_view)
    LinearLayout llCancelView;
    @BindView(R.id.tv_pay_amount)
    TextViewSFDisplayBold tvPayAmount;

    private OrderByIdModel.Data orderData;
    private OrderModel.Data singleData;
    private String[] rating = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

    public static OrderApplyFragment newInstanace(OrderModel.Data singleData) {
        OrderApplyFragment fragment = new OrderApplyFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ORDER_DATA, singleData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_apply, container, false);
        ButterKnife.bind(this, v);

        init();
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (((OrderDetailsActivity) activity).isAvailableOrders()) {
                llApplyView.setVisibility(View.VISIBLE);
            } else {
                llCancelView.setVisibility(View.VISIBLE);
            }
        }
    }

    public void init() {
        if (activity != null) {
            orderData = ((OrderDetailsActivity) activity).getOrderData();
        }

        if (getArguments() != null) {
            singleData = (OrderModel.Data) getArguments().getSerializable(Constants.ORDER_DATA);
        }

        if (orderData != null && singleData != null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>
                    (activity, android.R.layout.select_dialog_item, rating);
            tvRating.setThreshold(1);
            tvRating.setAdapter(adapter);

            tvPayAmount.setText("$" + orderData.total);
        }
    }

    @OnClick({R.id.tv_rating, R.id.rl_pay, R.id.tv_cancel_application})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_rating:
                tvRating.showDropDown();
                break;
            case R.id.rl_pay:
                if (!isValid()) {
                    showAlertDialog();
                } else {
                    applyJob();
                }
                break;
            case R.id.tv_cancel_application:
                showCancelOrderDialog();
                break;
        }
    }

    public String getComments() {
        return etComments.getText().toString().trim();
    }

    public String getRating() {
        if (tvRating.getText().toString().equals(getString(R.string.give_your_rating))) {
            return "";
        }
        return tvRating.getText().toString();
    }

    public void showAlertDialog() {
        final Dialog dialog = new Dialog(activity, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(getString(R.string.comment_rating_message));
        tvCancel.setText(getString(R.string.no));
        tvChatnow.setText(getString(R.string.yes));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                applyJob();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public boolean isValid() {
        if (activity.isEmpty(getComments())) {
            return false;
        }

        if (activity.isEmpty(getRating())) {
            return false;
        }

        return true;
    }

    public void applyJob() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().applyJob(Constants.APPLY_JOB, activity.getUserId(), singleData.orderId,
                getComments(), getRating(), activity.getAccessToken());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {
                    Toast.makeText(activity, response.body().msg, Toast.LENGTH_SHORT).show();
                    llApplyView.setVisibility(View.GONE);
                    llCancelView.setVisibility(View.VISIBLE);
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("apply job failed");
            }
        });
    }

    private void showCancelOrderDialog() {
        final Dialog dialog = new Dialog(activity, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_cancel_order);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvNo = dialog.findViewById(R.id.tv_no);

        String s = getString(R.string.cancel_orders_text);
        int[] colorList = {R.color.black};
        String[] words = {"Cancel this order?"};
        String[] fonts = {Constants.SFTEXT_BOLD};
        tvMessage.setText(Utils.getBoldString(activity, s, fonts, colorList, words));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelApplication();
                dialog.dismiss();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void cancelApplication() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().cancelApplication(Constants.CANCEL_APPLICATION, activity.getUserId(),
                singleData.orderId, activity.getAccessToken());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {
                    Toast.makeText(activity, response.body().msg, Toast.LENGTH_SHORT).show();
                    llApplyView.setVisibility(View.VISIBLE);
                    llCancelView.setVisibility(View.GONE);
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("cancel failed");
            }
        });
    }
}
