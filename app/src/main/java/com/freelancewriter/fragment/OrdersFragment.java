package com.freelancewriter.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daimajia.swipe.util.Attributes;
import com.freelancewriter.R;
import com.freelancewriter.adapter.OrderAdapter;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersFragment extends BaseFragment {

    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_no_orders)
    TextViewSFDisplayBold tvNoOrders;

    private List<OrderModel.Data> orderArrayList;
    private OrderAdapter mAdapter;
    private String filterType = Constants.ALL;
    private String orderType;
    private boolean isPullToRefresh = false;
    private boolean isMyOrders = false;
    private boolean isScreenVisible = false;

    public static OrdersFragment newInstanace(String orderType, boolean isMyOrders) {
        OrdersFragment fragment = new OrdersFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ORDER_TYPE, orderType);
        bundle.putBoolean(Constants.IS_MY_ORDER, isMyOrders);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, v);

        init();
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isScreenVisible = isVisibleToUser;
    }

    public void init() {
        if (getArguments() != null) {
            orderType = getArguments().getString(Constants.ORDER_TYPE);
            isMyOrders = getArguments().getBoolean(Constants.IS_MY_ORDER);
        }

        switch (orderType) {
            case Constants.ALL:
                filterType = isMyOrders ? Constants.GET_MY_ORDERS : Constants.GET_NEW_ORDERS;
                break;
            case Constants.AVAILABLE:
                filterType = Constants.GET_AVAILABLE_ORDERS;
                break;
            case Constants.FOR_BID:
                filterType = Constants.GET_ORDERS_FOR_BID;
                break;
            case Constants.CURRENT:
                filterType = Constants.GET_CURRENT_ORDERS;
                break;
            case Constants.COMPLETED:
                filterType = Constants.GET_COMPLETED_ORDERS;
                break;
            case Constants.CANCELLED:
                filterType = Constants.GET_CANCELED_ORDERS;
                break;
            case Constants.REVISION:
                filterType = Constants.GET_REVISION_ORDERS;
                break;
        }
        orderArrayList = new ArrayList<>();
        rvOrders.setLayoutManager(new LinearLayoutManager(activity));
        getOrders(filterType);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isPullToRefresh = true;
                getOrders(filterType);
            }
        });
    }

    public void getOrders(String method) {
        if (!activity.isNetworkConnected()) {
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }
            return;
        }

        if (!isPullToRefresh)
            activity.showProgress();

        Call<Object> orderCall = activity.getService().getOrder(method, activity.getUserId(), activity.getAccessToken());
        orderCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                try {
                    isPullToRefresh = false;
                    orderArrayList = new ArrayList<>();
                    String jsonResposne = new Gson().toJson(response.body());
                    if (activity.checkStatus(jsonResposne)) {
                        Log.e("Orders Response", jsonResposne);
                        OrderModel orderModel = activity.getModel(jsonResposne, OrderModel.class);
                        orderArrayList = orderModel.data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setAdapter();
                activity.hideProgress();
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                activity.failureError("Order failed");
                isPullToRefresh = false;
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public void setAdapter() {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            tvNoOrders.setVisibility(View.GONE);
            if (mAdapter == null) {
                mAdapter = new OrderAdapter(activity);
                mAdapter.setMode(Attributes.Mode.Single);
            }

            mAdapter.doRefresh(orderArrayList, filterType);

            if (rvOrders != null && rvOrders.getAdapter() == null) {
                rvOrders.setAdapter(mAdapter);
            }
        } else {
            if (mAdapter != null) {
                mAdapter.doRefresh(null, filterType);
            }
            tvNoOrders.setVisibility(View.VISIBLE);
            Toast.makeText(activity, "No orders found", Toast.LENGTH_SHORT).show();
        }
    }
}
