package com.freelancewriter.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ahamed.multiviewadapter.SimpleRecyclerAdapter;
import com.freelancewriter.R;
import com.freelancewriter.adapter.TypesAdapter;
import com.freelancewriter.adapter.WriterFileAdapter;
import com.freelancewriter.adapter.binder.FilesBinder;
import com.freelancewriter.model.FileUpload;
import com.freelancewriter.model.OrderByIdModel;
import com.freelancewriter.model.OrderByIdModel.WriterFiles;
import com.freelancewriter.model.TypesModel;
import com.freelancewriter.model.UserFiles;
import com.freelancewriter.ui.order.OrderDetailsActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Preferences;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.vincent.filepicker.filter.entity.VideoFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class OrderFilesFragment extends BaseFragment {

    @BindView(R.id.rv_my_files)
    RecyclerView rvMyFiles;
    @BindView(R.id.img_my_showall)
    TextViewSFDisplayRegular imgMyShowall;
    @BindView(R.id.rv_writer_files)
    RecyclerView rvWriterFiles;
    @BindView(R.id.img_writer_showall)
    TextViewSFDisplayRegular imgWriterShowall;
    @BindView(R.id.ll_writer_files)
    LinearLayout llWriterFiles;
    @BindView(R.id.tv_my_no_files)
    TextViewSFDisplayBold tvMyNoFiles;
    @BindView(R.id.tv_wr_no_files)
    TextViewSFDisplayBold tvWrNoFiles;

    private List<UserFiles> filesModelList;
    private List<WriterFiles> wrFilesModelList;
    private OrderByIdModel.Data orderData;
    private SimpleRecyclerAdapter myFilesAdapter;
    private boolean isMyFileShowAll = false;
    private boolean isWriterFileShowAll = false;
    private WriterFileAdapter writerFileAdapter;
    private ArrayList<File> fileList;

    public static OrderFilesFragment newInstanace(boolean isVisa) {
        OrderFilesFragment fragment = new OrderFilesFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_files, container, false);
        ButterKnife.bind(this, v);

        if (activity != null) {
            orderData = ((OrderDetailsActivity) activity).getOrderData();
        }

        if (orderData != null) {
            filesModelList = new ArrayList<>();
            wrFilesModelList = new ArrayList<>();

            imgMyShowall.setPaintFlags(imgMyShowall.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            rvMyFiles.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvWriterFiles.setLayoutManager(new LinearLayoutManager(getActivity()));
            setFileAdapter();
            setWriterFileAdapter();

            rvWriterFiles.setNestedScrollingEnabled(false);
            rvMyFiles.setNestedScrollingEnabled(false);
        }
        return v;
    }

    @OnClick({R.id.img_my_showall, R.id.img_writer_showall, R.id.img_add_file})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_my_showall:
                isMyFileShowAll = true;
                imgMyShowall.setVisibility(View.GONE);
                setFileAdapter();
                break;
            case R.id.img_writer_showall:
                isWriterFileShowAll = true;
                imgWriterShowall.setVisibility(View.GONE);
                setWriterFileAdapter();
                break;
            case R.id.img_add_file:
                selectFileDialog();
                break;
        }
    }

    public void selectFileDialog() {
        if (activity == null)
            return;

        final Dialog dialog = new Dialog(activity, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_camera_document_select);
        dialog.setCancelable(true);
        TextView tvCancel = dialog.findViewById(R.id.btn_cancel);
        LinearLayout llCamera = dialog.findViewById(R.id.ll_camera);
        LinearLayout llDocument = dialog.findViewById(R.id.ll_document);

        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(false);
                dialog.dismiss();
            }
        });

        llDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(true);
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void checkPermission(final boolean isDocument) {
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (isDocument) {
                                Intent intent = new Intent(activity, NormalFilePickActivity.class);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                intent.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
                            } else {
                                Intent intent = new Intent(activity, ImagePickActivity.class);
                                intent.putExtra(IS_NEED_CAMERA, true);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            Toast.makeText(activity, "Please give permission", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fileList = new ArrayList<>();
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<NormalFile> docPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    if (docPaths != null && docPaths.size() > 0) {
                        for (NormalFile file: docPaths) {
                            fileList.add(new File(file.getPath()));
                        }
                        Log.e("Doc Path == > ", docPaths.get(0).getPath());
                        uploadFile();
                    } else {
                        Toast.makeText(activity, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<ImageFile> imgPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    if (imgPath != null && imgPath.size() > 0) {
                        for (ImageFile file: imgPath) {
                            fileList.add(new File(file.getPath()));
                        }
                        Log.e("Image Path == > ", imgPath.get(0).getPath());
                        uploadFile();
                    } else {
                        Toast.makeText(activity, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    public void uploadFile() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        MultipartBody.Part[] body = null;
        if (fileList != null && fileList.size() > 0) {
            body = new MultipartBody.Part[fileList.size()];
            for (int i = 0; i < fileList.size(); i++) {
                File file = fileList.get(i);
                Uri selectedUri = Uri.fromFile(file);
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

                RequestBody requestFile = null;
                if (mimeType != null) {
                    requestFile = RequestBody.create(MediaType.parse(mimeType), file);
                }

                if (requestFile != null) {
                    body[i] = MultipartBody.Part.createFormData("admin_upload_paper[]", file.getName(), requestFile);
                }
            }
        }

        RequestBody method = RequestBody.create(MultipartBody.FORM, Constants.UPLOAD_FILE);
        RequestBody order_id = RequestBody.create(MultipartBody.FORM, orderData.orderId);
        RequestBody user_id = RequestBody.create(MultipartBody.FORM, activity.getUserId());
        RequestBody accesstoken = RequestBody.create(MultipartBody.FORM, activity.getAccessToken());

        Log.e("Upload file Url = > ", Constants.BASE_URL + Constants.UPLOAD_FILE);

        Call<FileUpload> call = activity.getService().uploadNewFile(method, body, order_id, user_id, accesstoken);
        call.enqueue(new Callback<FileUpload>() {
            @Override
            public void onResponse(Call<FileUpload> call, Response<FileUpload> response) {
                FileUpload fileUpload = response.body();
                List<UserFiles> userFilesList = new ArrayList<>();
                if (activity.checkStatus(fileUpload)) {
                    for (int i = 0; i < fileUpload.data.size(); i++) {
                        String jsonString = new Gson().toJson(fileUpload.data.get(i));
                        UserFiles userFiles = new Gson().fromJson(jsonString, UserFiles.class);
                        userFilesList.add(userFiles);
                    }
                }

                orderData.userFiles = userFilesList;
                setFileAdapter();
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<FileUpload> call, Throwable t) {
                activity.failureError("file upload failed");
            }
        });
    }

    public void setFileAdapter() {
        if (orderData.userFiles != null && orderData.userFiles.size() > 0) {
            filesModelList.clear();
            if (!isMyFileShowAll) {
                if (orderData.userFiles.size() > 3) {
                    for (int i = 0; i < 3; i++) {
                        filesModelList.add(orderData.userFiles.get(i));
                    }
                    imgMyShowall.setVisibility(View.VISIBLE);
                } else {
                    filesModelList.addAll(orderData.userFiles);
                }
            } else {
                filesModelList.addAll(orderData.userFiles);
            }

            if (myFilesAdapter == null) {
                myFilesAdapter = new SimpleRecyclerAdapter<>(new FilesBinder());
            }

            if (rvMyFiles.getAdapter() == null) {
                rvMyFiles.setAdapter(myFilesAdapter);
            }

            myFilesAdapter.setData(filesModelList);
            rvMyFiles.setVisibility(View.VISIBLE);
            tvMyNoFiles.setVisibility(View.GONE);
        } else {
            rvMyFiles.setVisibility(View.GONE);
            tvMyNoFiles.setVisibility(View.VISIBLE);
        }
    }

    public void setWriterFileAdapter() {
        if (orderData.writerFiles != null && orderData.writerFiles.size() > 0) {
            if (!isWriterFileShowAll) {
                if (orderData.writerFiles.size() > 3) {
                    for (int i = 0; i < 3; i++) {
                        wrFilesModelList.add(orderData.writerFiles.get(i));
                    }
                    imgWriterShowall.setVisibility(View.VISIBLE);
                } else {
                    wrFilesModelList.addAll(orderData.writerFiles);
                }
            } else {
                wrFilesModelList.addAll(orderData.writerFiles);
            }

            if (writerFileAdapter == null) {
                writerFileAdapter = new WriterFileAdapter(activity);
            }

            writerFileAdapter.doRefresh(wrFilesModelList);
            if (rvWriterFiles.getAdapter() == null) {
                rvWriterFiles.setAdapter(writerFileAdapter);
            }

            rvWriterFiles.setVisibility(View.VISIBLE);
            tvWrNoFiles.setVisibility(View.GONE);
        } else {
            rvWriterFiles.setVisibility(View.GONE);
            tvWrNoFiles.setVisibility(View.VISIBLE);
        }
    }
}
