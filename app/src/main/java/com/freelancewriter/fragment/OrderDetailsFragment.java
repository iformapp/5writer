package com.freelancewriter.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.ahamed.multiviewadapter.SimpleRecyclerAdapter;
import com.freelancewriter.R;
import com.freelancewriter.adapter.binder.OrderBinder;
import com.freelancewriter.model.OrderByIdModel;
import com.freelancewriter.model.OrderDetailsModel;
import com.freelancewriter.ui.order.OrderDetailsActivity;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;
import com.freelancewriter.util.textview.TextViewSFTextBold;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailsFragment extends BaseFragment {

    @BindView(R.id.rv_order_details)
    RecyclerView rvOrderDetails;
    @BindView(R.id.rv_page_style)
    RecyclerView rvPageStyle;
    @BindView(R.id.rv_page_option)
    RecyclerView rvPageOption;
    @BindView(R.id.rv_order_type)
    RecyclerView rvOrderType;
    @BindView(R.id.tv_info)
    TextViewSFDisplayRegular tvInfo;
    @BindView(R.id.tv_topic)
    TextViewSFTextBold tvTopic;

    private List<OrderDetailsModel> detailsModelList;
    private List<OrderDetailsModel> pageStyleList;
    private List<OrderDetailsModel> pageOptionList;
    private List<OrderDetailsModel> orderTypeList;
    private OrderByIdModel.Data orderData;

    public static OrderDetailsFragment newInstanace(boolean isVisa) {
        OrderDetailsFragment fragment = new OrderDetailsFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_details, container, false);
        ButterKnife.bind(this, v);

        if (getActivity() != null) {
            orderData = ((OrderDetailsActivity) getActivity()).getOrderData();
        }

        if (orderData != null) {
            detailsModelList = new ArrayList<>();
            pageStyleList = new ArrayList<>();
            pageOptionList = new ArrayList<>();
            orderTypeList = new ArrayList<>();

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> orderDetailsAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvOrderDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvOrderDetails.setAdapter(orderDetailsAdapter);
            orderDetailsAdapter.setData(fillOrderList());

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> pageStyleAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvPageStyle.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvPageStyle.setAdapter(pageStyleAdapter);
            pageStyleAdapter.setData(fillPageStyleList());

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> pageOptionAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvPageOption.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvPageOption.setAdapter(pageOptionAdapter);
            pageOptionAdapter.setData(fillPageOptionList());

            SimpleRecyclerAdapter<OrderDetailsModel, OrderBinder> orderTypeAdapter =
                    new SimpleRecyclerAdapter<>(new OrderBinder());

            rvOrderType.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvOrderType.setAdapter(orderTypeAdapter);
            orderTypeAdapter.setData(fillOrderTypeList());

            rvPageOption.setNestedScrollingEnabled(false);
            rvPageStyle.setNestedScrollingEnabled(false);
            rvOrderDetails.setNestedScrollingEnabled(false);
            rvOrderType.setNestedScrollingEnabled(false);

            tvTopic.setText(orderData.topic);
            tvInfo.setText(Utils.fromHtml(orderData.additionalDetail));
            tvInfo.setMovementMethod(new ScrollingMovementMethod());
            tvInfo.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                    return false;
                }
            });
        }
        return v;
    }

    public List fillOrderList() {
        detailsModelList.add(new OrderDetailsModel(getString(R.string.type_of_service), orderData.service, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.writer_level), orderData.academic, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.type_of_paper), orderData.paperName, "", false));
        if (!TextUtils.isEmpty(orderData.otherPaperName)) {
            detailsModelList.add(new OrderDetailsModel(getString(R.string.other_paper_type), orderData.otherPaperName, "", false));
        }
        detailsModelList.add(new OrderDetailsModel(getString(R.string.deadline), orderData.deadline + " ("
                + Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd hh:mm", orderData.wrDeadline) + ")", "", false));
        String pages = "";
        if (orderData.space.equalsIgnoreCase("2")) {
            pages = "560 words";
        } else if (orderData.space.equalsIgnoreCase("1")) {
            pages = "280 words";
        }
        detailsModelList.add(new OrderDetailsModel(getString(R.string.pages), orderData.pages + " / " + pages, "", false));
        detailsModelList.add(new OrderDetailsModel(getString(R.string.subject), orderData.subjectName, "", false));
        if (!TextUtils.isEmpty(orderData.otherSubject)) {
            detailsModelList.add(new OrderDetailsModel(getString(R.string.other_subject_name), orderData.otherSubject, "", false));
        }
//        detailsModelList.add(new OrderDetailsModel(getString(R.string.topic), TextUtils.isEmpty(orderData.topic) ? "Writer's Choice..." : orderData.topic,
//                "", false));
        return detailsModelList;
    }

    public List fillPageStyleList() {
        pageStyleList.add(new OrderDetailsModel(getString(R.string.preferred_writer), orderData.writer, "", false));
        pageStyleList.add(new OrderDetailsModel(getString(R.string.format_style), orderData.styleName, "", false));
        pageStyleList.add(new OrderDetailsModel(getString(R.string.discipline), orderData.discipline, "", false));
        return pageStyleList;
    }

    public List fillPageOptionList() {
        pageOptionList.add(new OrderDetailsModel(getString(R.string.turnitin_plagiarism_report), "", getString(R.string.turnitin_price),
                orderData.isPlagiarismReport.equalsIgnoreCase("1")));
        pageOptionList.add(new OrderDetailsModel(getString(R.string.abstract_page), "", getString(R.string.abstract_price),
                orderData.isAbstructPage.equalsIgnoreCase("1")));
        pageOptionList.add(new OrderDetailsModel(getString(R.string.send_it_to_my_e_mail), "", getString(R.string.send_email_price),
                orderData.isSendToMyEmail.equalsIgnoreCase("1")));
        return pageOptionList;
    }

    public List fillOrderTypeList() {
        orderTypeList.add(new OrderDetailsModel(getString(R.string.sorces), orderData.source, "", false));
        orderTypeList.add(new OrderDetailsModel(getString(R.string.charts), orderData.chart, "", false));
        orderTypeList.add(new OrderDetailsModel(getString(R.string.powerpoint_slide), orderData.slides, "", false));
        return orderTypeList;
    }
}
