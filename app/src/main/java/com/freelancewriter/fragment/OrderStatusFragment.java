package com.freelancewriter.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancewriter.BuildConfig;
import com.freelancewriter.R;
import com.freelancewriter.adapter.RecyclerviewAdapter;
import com.freelancewriter.model.CompletedFiles;
import com.freelancewriter.model.GeneralModel;
import com.freelancewriter.model.OrderByIdModel;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.order.OrderDetailsActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.MyDownloadManager;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;
import com.freelancewriter.util.textview.TextViewSFTextBold;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatusFragment extends BaseFragment implements RecyclerviewAdapter.OnViewBindListner {

    @Nullable
    @BindView(R.id.tv_days)
    TextViewSFDisplayBold tvDays;
    @Nullable
    @BindView(R.id.tv_hours)
    TextViewSFDisplayBold tvHours;
    @Nullable
    @BindView(R.id.tv_minutes)
    TextViewSFDisplayBold tvMinutes;
    @Nullable
    @BindView(R.id.tv_second)
    TextViewSFDisplayBold tvSecond;
    @Nullable
    @BindView(R.id.rv_files)
    RecyclerView rvFiles;
    @Nullable
    @BindView(R.id.tv_file_name)
    TextViewSFDisplayBold tvFileName;
    @Nullable
    @BindView(R.id.tv_date)
    TextViewSFDisplayRegular tvDate;
    @Nullable
    @BindView(R.id.tv_wr_id)
    TextViewSFDisplayBold tvWrId;
    @Nullable
    @BindView(R.id.img_menu)
    ImageView imgMenu;
    @Nullable
    @BindView(R.id.tv_order_status)
    TextViewSFDisplayBold tvOrderStatus;
    @Nullable
    @BindView(R.id.tv_payment_status)
    TextViewSFDisplayBold tvPaymentStatus;
    @Nullable
    @BindView(R.id.ll_timer)
    LinearLayout llTimer;
    @Nullable
    @BindView(R.id.tv_no_files)
    TextViewSFDisplayBold tvNoFiles;
    @BindView(R.id.tv_order_no)
    TextViewSFTextBold tvOrderNo;
    @BindView(R.id.tv_canceled_order)
    TextViewSFDisplayBold tvCanceledOrder;
    @BindView(R.id.tv_price_per_page)
    TextViewSFDisplayRegular tvPricePerPage;
    @BindView(R.id.tv_total_pages)
    TextViewSFDisplayRegular tvTotalPages;
    @BindView(R.id.tv_total)
    TextViewSFDisplayBold tvTotal;
    @BindView(R.id.ll_cost)
    LinearLayout llCost;
    @BindView(R.id.tv_available_status)
    TextViewSFTextBold tvAvailableStatus;
    @BindView(R.id.ll_apply_cancel)
    LinearLayout llApplyCancel;
    @BindView(R.id.ll_status_button)
    LinearLayout llStatusButton;
    @BindView(R.id.tv_cancel_application)
    TextViewSFDisplayBold tvCancelApplication;
    @BindView(R.id.tv_time_left)
    TextViewSFDisplayBold tvTimeLeft;

    private RecyclerviewAdapter mAdapter;
    private OrderByIdModel.Data orderData;
    private List<CompletedFiles.Data> completedFiles;
    private String orderType;
    private String filterType;

    public static OrderStatusFragment newInstanace(OrderModel.Data singleData) {
        OrderStatusFragment fragment = new OrderStatusFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ORDER_DATA, singleData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_status, container, false);
        ButterKnife.bind(this, v);

        if (activity != null) {
            orderData = ((OrderDetailsActivity) activity).getOrderData();
            orderType = ((OrderDetailsActivity) activity).orderType();
            filterType = ((OrderDetailsActivity) activity).getFilterType();
        }

        // According to Order status Change Button color
        if (orderData != null) {
            if (rvFiles != null) {
                rvFiles.setLayoutManager(new LinearLayoutManager(activity));
            }
            if (orderData.orderStatusName.equalsIgnoreCase(Constants.ORDER_AWARDED)) {
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.blue_button_bg));
                }
                setTimerText();
            } else if (orderData.orderStatusName.equalsIgnoreCase(Constants.ORDER_PROCESSING)) {
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.blue_button_bg));
                }
                setTimerText();
            } else if (orderData.orderStatusName.equalsIgnoreCase(Constants.ORDER_COMPLETED)) {
                if (llTimer != null) {
                    llTimer.setVisibility(View.GONE);
                }
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.paid_button_bg));
                }
                //getCompletedFiles();
            } else if (orderData.orderStatusName.equalsIgnoreCase(Constants.ORDER_CANCELLED)) {
                llTimer.setVisibility(View.GONE);
                tvCanceledOrder.setVisibility(View.VISIBLE);
                llCost.setVisibility(View.GONE);
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                }
            } else if (orderData.orderStatusName.equalsIgnoreCase(Constants.ORDER_WAITING_FOR_PAYMENT)) {
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                }
                setTimerText();
            } else {
                if (tvOrderStatus != null) {
                    tvOrderStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                }
                setTimerText();
            }

            // Check Available and Forbid Orders and Change it's UI
            if (orderType != null && !TextUtils.isEmpty(orderType)) {
                switch (orderType) {
                    case Constants.AVAILABLE_ORDER:
                        llApplyCancel.setVisibility(View.VISIBLE);
                        llStatusButton.setVisibility(View.GONE);
                        tvAvailableStatus.setText(getString(R.string.apply));
                        tvAvailableStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.green_button_bg));
                        tvAvailableStatus.setTextColor(ContextCompat.getColor(activity, R.color.white));
                        break;
                    case Constants.FORBID_ORDER:
                        llApplyCancel.setVisibility(View.VISIBLE);
                        llStatusButton.setVisibility(View.GONE);
                        tvAvailableStatus.setText(getString(R.string.for_bid));
                        tvAvailableStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.yellow_button_bg_20));
                        tvAvailableStatus.setTextColor(ContextCompat.getColor(activity, R.color.black));
                        break;
                }

                // For Current Order, View Canceled Order Button
                if (orderType.equals(Constants.CURRENT_ORDER)) {
                    currentOrderView();
                }
            }

            // For Current Order, View Canceled Order Button
            if (filterType.equals(Constants.GET_CURRENT_ORDERS)) {
                currentOrderView();
            }

            tvOrderNo.setText(orderData.orderNumber);
            tvPricePerPage.setText("$" + orderData.costPerPage);
            tvTotalPages.setText(orderData.pages);
            tvTotal.setText("$" + orderData.total);
            if (tvOrderStatus != null) {
                tvOrderStatus.setText(orderData.orderStatusName);
            }

            // Set Payment Status
            switch (orderData.paymentStatus) {
                case "1":
                    tvPaymentStatus.setText(getString(R.string.unpaid));
                    tvPaymentStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                    break;
                case "2":
                    tvPaymentStatus.setText(getString(R.string.paid));
                    tvPaymentStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.paid_button_bg));
                    break;
                case "3":
                    tvPaymentStatus.setText(getString(R.string.hold));
                    tvPaymentStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.blue_button_bg));
                    break;
                default:
                    tvPaymentStatus.setText(getString(R.string.unassign));
                    tvPaymentStatus.setBackground(ContextCompat.getDrawable(activity, R.drawable.waiting_button_bg));
                    break;
            }
        }
        return v;
    }

    private void currentOrderView() {
        tvCancelApplication.setVisibility(View.VISIBLE);
        if (orderData.isDue) {
            tvTimeLeft.setText("Late Time");
            setRedViews(tvTimeLeft, tvDays, tvHours, tvMinutes, tvSecond);
        }
    }

    public void setRedViews(View... views) {
        for (View v : views) {
            ((TextView) v).setTextColor(ContextCompat.getColor(activity, R.color.reset_pw_btn));
        }
    }

    @OnClick(R.id.tv_cancel_application)
    public void onViewClicked() {
        showCancelOrderDialog();
    }

    private void showCancelOrderDialog() {
        final Dialog dialog = new Dialog(activity, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_cancel_order);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvNo = dialog.findViewById(R.id.tv_no);

        String s = getString(R.string.cancel_fee_orders_text);
        int[] colorList = {R.color.red, R.color.black};
        String[] words = {"fees", "Cancel this order?"};
        String[] fonts = {Constants.SFTEXT_REGULAR, Constants.SFTEXT_BOLD};
        tvMessage.setText(Utils.getBoldString(activity, s, fonts, colorList, words));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelApplication();
                dialog.dismiss();
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void setTimerText() {
        try {
            int day = Integer.parseInt(orderData.dayint);
            int hour = Integer.parseInt(orderData.hoursint);
            int minute = Integer.parseInt(orderData.minutes);
            int second = Integer.parseInt(orderData.seconds);

            if (filterType.equals(Constants.GET_CURRENT_ORDERS) || orderType.equals(Constants.CURRENT_ORDER)) {
                int tempSec = (day * (24 * 60 * 60)) + (hour * 60 * 60) + (minute * 60) + second;

                if (tempSec > 0) {
                    new CountDownTimer(tempSec * 1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                            long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(days);
                            long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished));
                            long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));
                            setTimerUi((int) days, (int) hours, (int) minutes, (int) seconds);
                        }

                        @Override
                        public void onFinish() {
                        }
                    }.start();
                }
            } else {
                setTimerUi(day, hour, minute, second);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTimerUi(int day, int hour, int minute, int second) {
        if (tvHours != null) {
            tvHours.setText(Utils.doubleDigit(hour));
        }
        if (tvDays != null) {
            tvDays.setText(Utils.doubleDigit(day));
        }
        if (tvMinutes != null) {
            tvMinutes.setText(Utils.doubleDigit(minute));
        }
        if (tvSecond != null) {
            tvSecond.setText(Utils.doubleDigit(second));
        }
    }

    public void cancelApplication() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().cancelApplication(Constants.CANCEL_APPLICATION, activity.getUserId(),
                orderData.orderId, activity.getAccessToken());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (activity.checkStatus(response.body())) {
                    Toast.makeText(activity, response.body().msg, Toast.LENGTH_SHORT).show();
                    ((OrderDetailsActivity) activity).cancelOrderUi();
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("cancel failed");
            }
        });
    }

    public void getCompletedFiles() {
        if (!activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<CompletedFiles> call = activity.getService().getCompletedFiles(Constants.FILES_BY_ORDER, activity.getUserId(),
                activity.getAccessToken(), orderData.orderId);
        call.enqueue(new Callback<CompletedFiles>() {
            @Override
            public void onResponse(Call<CompletedFiles> call, Response<CompletedFiles> response) {
                CompletedFiles model = response.body();
                if (activity.checkStatus(model)) {
                    completedFiles = model.data;
                    if (completedFiles != null && completedFiles.size() > 0) {
                        if (tvNoFiles != null) {
                            tvNoFiles.setVisibility(View.GONE);
                        }
                        if (rvFiles != null) {
                            rvFiles.setVisibility(View.VISIBLE);
                        }
                        mAdapter = new RecyclerviewAdapter((ArrayList<?>) completedFiles, R.layout.item_status_complete_files,
                                OrderStatusFragment.this);
                        rvFiles.setAdapter(mAdapter);
                    } else {
                        if (tvNoFiles != null) {
                            tvNoFiles.setVisibility(View.VISIBLE);
                        }
                        if (rvFiles != null) {
                            rvFiles.setVisibility(View.GONE);
                        }
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<CompletedFiles> call, Throwable t) {
                activity.failureError("completed file failed");
            }
        });
    }

    @Override
    public void bindView(View view, final int position) {
        ButterKnife.bind(this, view);

        final CompletedFiles.Data item = completedFiles.get(position);
        if (tvFileName != null) {
            tvFileName.setText(item.fileName);
        }
        if (tvDate != null) {
            tvDate.setText(Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd/MM/yyyy", item.dateTime));
        }
        if (tvWrId != null) {
            //tvWrId.setText(TextUtils.isEmpty(item.writerId) ? "" : item.writerId);
        }
        if (imgMenu != null) {
            imgMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showOptionDialog(item);
                }
            });
        }
    }

    private void showOptionDialog(final CompletedFiles.Data item) {
        final Dialog dialog = new Dialog(activity, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_file_option_menu);
        dialog.setCancelable(true);

        View llView = dialog.findViewById(R.id.ll_view);
        View llDownload = dialog.findViewById(R.id.ll_download);
        View llEmail = dialog.findViewById(R.id.ll_email);
        View llShare = dialog.findViewById(R.id.ll_share);
        TextView btnCancel = dialog.findViewById(R.id.btn_cancel);

        llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, false, false, false);
            }
        });

        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, true, false, false);
            }
        });

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, false, false, true);
            }
        });

        llEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkPermission(item, false, true, false);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void downloadFile(final CompletedFiles.Data item, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        File folder = new File(Environment.getExternalStorageDirectory(), "/Download/" + activity.getString(R.string.app_name));
        if (!folder.exists())
            folder.mkdir();

        final File file = new File(folder, item.fileName);
        if (!file.exists()) {
            activity.showProgress();

            MyDownloadManager downloadManager = new MyDownloadManager(activity)
                    .setDownloadUrl(item.filePath)
                    .setTitle(item.fileName)
                    .setDestinationUri(file)
                    .setDownloadCompleteListener(new MyDownloadManager.DownloadCompleteListener() {
                        @Override
                        public void onDownloadComplete() {
                            activity.hideProgress();
                            showOutput("Download complete", isDownload, file, isEmailShare, isShare);
                        }

                        @Override
                        public void onDownloadFailure() {
                            activity.hideProgress();
                            Toast.makeText(activity, "Download failed", Toast.LENGTH_SHORT).show();
                        }
                    });
            downloadManager.startDownload();
        } else {
            showOutput("Already Downloaded", isDownload, file, isEmailShare, isShare);
        }
    }

    private void showOutput(String message, boolean isDownload, File file, boolean isEmailShare, boolean isShare) {
        if (!isDownload) {
            if (isShare || isEmailShare)
                shareFile(file, isEmailShare);
            else
                viewFile(file);
        } else {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void viewFile(File file) {
        try {
            Uri uri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String mime = "*/*";
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String extension = mimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (mimeTypeMap.hasExtension(extension))
                mime = mimeTypeMap.getMimeTypeFromExtension(extension);
            intent.setDataAndType(uri, mime);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareFile(File file, boolean isEmailShare) {
        Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
        Intent intentShareFile = new Intent(isEmailShare ? Intent.ACTION_SENDTO : Intent.ACTION_SEND);
        intentShareFile.setType("*/*");
        if (isEmailShare) {
            intentShareFile.setData(Uri.parse("mailto:"));
            intentShareFile.putExtra(Intent.EXTRA_EMAIL, "");
        }
        intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);
        activity.startActivity(Intent.createChooser(intentShareFile, "Share File"));
    }

    private void checkPermission(final CompletedFiles.Data item, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        downloadFile(item, isDownload, isEmailShare, isShare);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(activity, "give storage permission first", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
}
