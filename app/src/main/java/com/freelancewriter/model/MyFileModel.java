package com.freelancewriter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyFileModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data {
        @Expose
        @SerializedName("date_added")
        public String dateAdded;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("comment")
        public String comment;
        @Expose
        @SerializedName("is_completed")
        public String isCompleted;
        @Expose
        @SerializedName("file_word_no")
        public String fileWordNo;
        @Expose
        @SerializedName("file_name")
        public String fileName;
        @Expose
        @SerializedName("src")
        public String src;
        @Expose
        @SerializedName("attachment_id")
        public int attachmentId;
        @Expose
        @SerializedName("msg")
        public String msg;
        @Expose
        @SerializedName("ok")
        public boolean ok;
    }
}
