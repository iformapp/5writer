package com.freelancewriter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("writer_id")
        public String writerId;
        @Expose
        @SerializedName("accesstoken")
        public String accesstoken;
        @Expose
        @SerializedName("first_name")
        public String firstName;
        @Expose
        @SerializedName("last_name")
        public String lastName;
        @Expose
        @SerializedName("country")
        public String country;
        @Expose
        @SerializedName("timezone")
        public String timezone;
        @Expose
        @SerializedName("mobile")
        public String mobile;
        @Expose
        @SerializedName("telephone")
        public String telephone;
        @Expose
        @SerializedName("telephone_prefix")
        public String dialcode;
        @Expose
        @SerializedName("password")
        public String password;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("method")
        public String method;
        @Expose
        @SerializedName("iso_code")
        public String isoCode;
        @Expose
        @SerializedName("balance")
        public String balance;
        @Expose
        @SerializedName("unpaid_balance")
        public String unpaidBalance;
    }
}
