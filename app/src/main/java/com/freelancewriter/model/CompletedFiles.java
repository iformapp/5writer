package com.freelancewriter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CompletedFiles extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data implements Serializable {

        @Expose
        @SerializedName("is_completed")
        public String isCompleted;
        @Expose
        @SerializedName("file_content")
        public String fileContent;
        @Expose
        @SerializedName("file_type")
        public String fileType;
        @Expose
        @SerializedName("file_size")
        public String fileSize;
        @Expose
        @SerializedName("category")
        public String category;
        @Expose
        @SerializedName("uploaded_by")
        public String uploadedBy;
        @Expose
        @SerializedName("date_time")
        public String dateTime;
        @Expose
        @SerializedName("file_path")
        public String filePath;
        @Expose
        @SerializedName("id")
        public String id;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("hide_from_writer")
        public String hideFromWriter;
        @Expose
        @SerializedName("approve")
        public String approve;
        @Expose
        @SerializedName("acct_id")
        public String acctId;
        @Expose
        @SerializedName("word_count")
        public String wordCount;
        @Expose
        @SerializedName("file_name")
        public String fileName;
        @Expose
        @SerializedName("user_id")
        public String userId;
    }
}
