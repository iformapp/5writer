package com.freelancewriter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderByIdModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("data")
    public Data data;

    public static class Data implements Serializable {
        @Expose
        @SerializedName("isDue")
        public Boolean isDue;
        @Expose
        @SerializedName("cost_per_page")
        public String costPerPage;
        @Expose
        @SerializedName("order_date")
        public String orderDate;
        @Expose
        @SerializedName("hoursint")
        public String hoursint;
        @Expose
        @SerializedName("minutes")
        public String minutes;
        @Expose
        @SerializedName("seconds")
        public String seconds;
        @Expose
        @SerializedName("dayint")
        public String dayint;
        @Expose
        @SerializedName("writer")
        public String writer;
        @Expose
        @SerializedName("order_status_description_id")
        public String orderStatusDescriptionId;
        @Expose
        @SerializedName("payment_status")
        public String paymentStatus;
        @Expose
        @SerializedName("payment_method")
        public String paymentMethod;
        @Expose
        @SerializedName("transaction_id")
        public String transactionId;
        @Expose
        @SerializedName("date_added")
        public String dateAdded;
        @Expose
        @SerializedName("total")
        public String total;
        @Expose
        @SerializedName("sub_total")
        public String subTotal;
        @Expose
        @SerializedName("coupon_id")
        public String couponId;
        @Expose
        @SerializedName("coupon_discount")
        public String couponDiscount;
        @Expose
        @SerializedName("deadline_id")
        public String deadlineId;
        @Expose
        @SerializedName("chart")
        public String chart;
        @Expose
        @SerializedName("is_send_to_my_email")
        public String isSendToMyEmail;
        @Expose
        @SerializedName("is_abstruct_page")
        public String isAbstructPage;
        @Expose
        @SerializedName("is_plagiarism_report")
        public String isPlagiarismReport;
        @Expose
        @SerializedName("slides")
        public String slides;
        @Expose
        @SerializedName("pages")
        public String pages;
        @Expose
        @SerializedName("space")
        public String space;
        @Expose
        @SerializedName("source")
        public String source;
        @Expose
        @SerializedName("other_paper_name")
        public String otherPaperName;
        @Expose
        @SerializedName("other_subject")
        public String otherSubject;
        @Expose
        @SerializedName("duration")
        public String duration;
        @Expose
        @SerializedName("period")
        public String period;
        @Expose
        @SerializedName("customer_name")
        public String customerName;
        @Expose
        @SerializedName("customer_email")
        public String customerEmail;
        @Expose
        @SerializedName("service")
        public String service;
        @Expose
        @SerializedName("topic")
        public String topic;
        @Expose
        @SerializedName("order_status_name")
        public String orderStatusName;
        @Expose
        @SerializedName("deadline")
        public String deadline;
        @Expose
        @SerializedName("wr_deadline")
        public String wrDeadline;
        @Expose
        @SerializedName("paper_name")
        public String paperName;
        @Expose
        @SerializedName("subject_name")
        public String subjectName;
        @Expose
        @SerializedName("style_name")
        public String styleName;
        @Expose
        @SerializedName("discipline")
        public String discipline;
        @Expose
        @SerializedName("academic")
        public String academic;
        @Expose
        @SerializedName("paper_id")
        public String paperId;
        @Expose
        @SerializedName("subject")
        public String subject;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("additional_detail")
        public String additionalDetail;
        @Expose
        @SerializedName("order_number")
        public String orderNumber;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("myFiles")
        public List<UserFiles> userFiles;
        @Expose
        @SerializedName("customersFiles")
        public List<WriterFiles> writerFiles;
    }

    public static class WriterFiles implements Serializable {

        @Expose
        @SerializedName("file_content")
        public String fileContent;
        @Expose
        @SerializedName("file_type")
        public String fileType;
        @Expose
        @SerializedName("file_size")
        public String fileSize;
        @Expose
        @SerializedName("acct_id")
        public String acctId;
        @Expose
        @SerializedName("hide_from_writer")
        public String hideFromWriter;
        @Expose
        @SerializedName("approve")
        public String approve;
        @Expose
        @SerializedName("word_count")
        public String wordCount;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("uploaded_by")
        public String uploadedBy;
        @Expose
        @SerializedName("date_time")
        public String dateTime;
        @Expose
        @SerializedName("category")
        public String category;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("file_path")
        public String filePath;
        @Expose
        @SerializedName("file_name")
        public String fileName;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
