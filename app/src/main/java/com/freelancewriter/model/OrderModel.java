package com.freelancewriter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderModel extends GeneralModel implements Serializable {

    @Expose
    @SerializedName("balances")
    public Balances balances;
    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Balances {
        @Expose
        @SerializedName("total_balance")
        public int totalBalance;
        @Expose
        @SerializedName("unpaid_balance")
        public int unpaidBalance;
        @Expose
        @SerializedName("paid_balance")
        public int paidBalance;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("order_date")
        public String orderDate;
        @Expose
        @SerializedName("order_type")
        public String orderType;
        @Expose
        @SerializedName("writer")
        public String writer;
        @Expose
        @SerializedName("order_status_description_id")
        public String orderStatusDescriptionId;
        @Expose
        @SerializedName("payment_status")
        public String paymentStatus;
        @Expose
        @SerializedName("payment_method")
        public String paymentMethod;
        @Expose
        @SerializedName("transaction_id")
        public String transactionId;
        @Expose
        @SerializedName("date_added")
        public String dateAdded;
        @Expose
        @SerializedName("total")
        public String total;
        @Expose
        @SerializedName("sub_total")
        public String subTotal;
        @Expose
        @SerializedName("coupon_id")
        public String couponId;
        @Expose
        @SerializedName("coupon_discount")
        public String couponDiscount;
        @Expose
        @SerializedName("deadline_id")
        public String deadlineId;
        @Expose
        @SerializedName("is_plagiarism_report")
        public String isPlagiarismReport;
        @Expose
        @SerializedName("slides")
        public String slides;
        @Expose
        @SerializedName("pages")
        public String pages;
        @Expose
        @SerializedName("space")
        public String space;
        @Expose
        @SerializedName("source")
        public String source;
        @Expose
        @SerializedName("other_paper_name")
        public String otherPaperName;
        @Expose
        @SerializedName("duration")
        public String duration;
        @Expose
        @SerializedName("period")
        public String period;
        @Expose
        @SerializedName("customer_name")
        public String customerName;
        @Expose
        @SerializedName("customer_email")
        public String customerEmail;
        @Expose
        @SerializedName("service")
        public String service;
        @Expose
        @SerializedName("order_status_name")
        public String orderStatusName;
        @Expose
        @SerializedName("deadline")
        public String deadline;
        @Expose
        @SerializedName("paper_name")
        public String paperName;
        @Expose
        @SerializedName("order_subject")
        public String orderSubject;
        @Expose
        @SerializedName("style_name")
        public String styleName;
        @Expose
        @SerializedName("discipline")
        public String discipline;
        @Expose
        @SerializedName("academic")
        public String academic;
        @Expose
        @SerializedName("paper_id")
        public String paperId;
        @Expose
        @SerializedName("subject")
        public String subject;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("additional_detail")
        public String additionalDetail;
        @Expose
        @SerializedName("order_number")
        public String orderNumber;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("hoursint")
        public String hoursint;
        @Expose
        @SerializedName("minutes")
        public String minutes;
        @Expose
        @SerializedName("seconds")
        public String seconds;
        @Expose
        @SerializedName("dayint")
        public String dayint;
        @Expose
        @SerializedName("isDue")
        public Boolean isDue;

        public boolean isSelected;
    }
}
