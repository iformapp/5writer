package com.freelancewriter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FileUpload extends GeneralModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;

    public static class Data {
        @Expose
        @SerializedName("filepath")
        public String filepath;
        @Expose
        @SerializedName("category")
        public String category;
        @Expose
        @SerializedName("user_id")
        public String userId;
        @Expose
        @SerializedName("file_upload_date")
        public String fileUploadDate;
        @Expose
        @SerializedName("file_upload_simple_date")
        public String fileUploadSimpleDate;
        @Expose
        @SerializedName("send_customer_by_mail_time")
        public String sendCustomerByMailTime;
        @Expose
        @SerializedName("date_added")
        public String dateAdded;
        @Expose
        @SerializedName("order_id")
        public String orderId;
        @Expose
        @SerializedName("file_name")
        public String fileName;
        @Expose
        @SerializedName("file_id")
        public String fileId;
        @Expose
        @SerializedName("file_path")
        public String filePath;
        @Expose
        @SerializedName("src")
        public String src;
        @Expose
        @SerializedName("attachment_id")
        public String attachmentId;
    }
}
