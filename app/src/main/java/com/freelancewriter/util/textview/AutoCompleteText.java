package com.freelancewriter.util.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.freelancewriter.util.Constants;

public class AutoCompleteText extends AutoCompleteTextView {

    public AutoCompleteText(Context context) {
        super(context);
        applyCustomFont();
    }

    public AutoCompleteText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public AutoCompleteText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFDISPLAY_REGULAR)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFDISPLAY_REGULAR);
            setTypeface(tf);
        }
    }
}
