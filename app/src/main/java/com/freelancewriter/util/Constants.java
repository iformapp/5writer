package com.freelancewriter.util;

public class Constants {

    //API URL PARAMS
    public static final String BASE_URL = "https://5writer.com/";
    public static final String URL = "api/webservice.php";
    public static final String MAIN_URL = BASE_URL + URL;
    public static final String FILE_PATH = BASE_URL + "common-upload/";
    public static final String LOGIN = "login";
    public static final String REGISTER = "signup";
    public static final String LOGOUT = "logout";
    public static final String APPLY_JOB = "applyJob";
    public static final String UPLOAD_PAPER = "uploadPaper";
    public static final String FILE_UPLOAD = "uploadPaper";
    public static final String UPDATE_PASSWORD = "updatePassword";
    public static final String SET_PASSWORD = "setPassword";
    public static final String CANCEL_APPLICATION = "cancelApplication";
    public static final String UPDATE_PROFILE = "updateProfile";
    public static final String SEND_FEEDBACK = "insertFeedback";
    public static final String UPLOAD_FILE = "uploadFile";
    public static final String FILES_BY_ORDER = "getFileByOrder";
    //public static final String UPDATE_PASSWORD = URL + "?method=setPasswordOnLoggedin";
    public static final String GET_ORDER = URL + "?method=getOrders";
    public static final String DELETE_ORDER = URL + "?method=deleteOrder";
    public static final String ORDER_DETAIL = "getOrdersById";
    public static final String ORDER_UPDATE = URL + "?method=updateOrderStatus";
    public static final String FORGOT_PASSWORD = "forgotPassword";
    public static final String PRICE_CALCULATE = URL + "?method=android_SetOrderOptionAll";
    //public static final String FILE_UPLOAD = URL + "?method=android_MyUploadMaterial";
    public static final String NEW_FILE_UPLOAD = URL + "?method=uploadNewFile";
    public static final String DELETE_UPLOAD = URL + "?method=android_MyDeleteMaterial";
    public static final String DELETE_ALL_FILES = URL + "?method=deleteUploadedFiles";
    public static final String ORDER_SAVE = URL + "?method=android_MyOrderSave";
    public static final String APPLY_COUPON = URL + "?method=android_UpdateDisc";
    public static final String REMOVE_COUPON = URL + "?method=android_MyRemoveDisc";

    public static final String REFUND = "money-back-guarantee.php";
    public static final String PRIVACY = "privacy-policy.php";
    public static final String REVISION_POLICY = "revision-policy.php";
    public static final String TERMS_USE = "terms-of-use.php";
    public static final String DISCLAIMER = "disclaimer.php";
    public static final String WEBSITE = "index.php";
    public static final String FAQS = "faqs.php";
    public static final String SERVICES = "services.php";
    public static final String PRICES = "price.php";

    //Api parameter
    public static final String PAPER_TYPES = "paperTypes";
    public static final String ACADEMIC_TYPES = "getAcademicTypes";
    public static final String DISCIPLIN_TYPES = "getDisciplines";
    public static final String FORMATED_STYLE_TYPES = "getFormatedStyle";
    public static final String SUBJECTS_TYPES = "subjects";
    public static final String CATEGORY_TYPES = "getCategory";

    public static final int TAB_HOME = 0;
    public static final int TAB_CHAT = 1;
    public static final int TAB_PLUS = 2;
    public static final int TAB_ORDER = 3;
    public static final int TAB_PROFILE = 4;

    public static final String SFTEXT_REGULAR = "font/SanFranciscoText-Regular.otf";
    public static final String SFTEXT_BOLD = "font/SanFranciscoText-Bold.otf";
    public static final String SFDISPLAY_BOLD = "font/SF-Pro-Display-Bold.otf";
    public static final String SFDISPLAY_REGULAR = "font/SF-Pro-Display-Regular.otf";

    public static final String FROM_LOGIN = "from login";
    public static final String IS_FINISH = "IsFinish";
    public static final String IS_VISA = "isVisa";
    public static final String IS_LOGIN = "isLogin";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String POLICY_URL = "policy_url";
    public static final String POLICY_TITLE = "policy_title";
    public static final String USER_DATA = "user_data";
    public static final String ORDER_DATA = "order_data";
    public static final String ORDER_ID = "order_id";
    public static final String SCREEN_NAME = "screen_name";
    public static final String DEADLINE_TYPE = "deadlineType";
    public static final String DEADLINE_VALUE = "deadlineValue";
    public static final String WRITER_LEVEL_ID = "writerLevelId";
    public static final String PAGE = "page";
    public static final String FILTER_TYPE = "filter_type_orders";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final int COLLEGE_ID = 4;
    public static final int BECHELOR_ID = 5;
    public static final int MASTER_ID = 6;

    public static final int WRITING_ID = 4;
    //public static final int EDITING_ID = 5;
    public static final int POWERPOINT_ID = 6;

    public static final int COMPLETED_ID = 1;
    public static final int EDITING_ID = 2;
    public static final int CANCEL_ID = 3;

    public static final int REQUEST_CODE_IMAGE = 23;

    public static final String GET_AVAILABLE_ORDERS = "getAvailableOrders";
    public static final String GET_CURRENT_ORDERS = "getCurrentOrders";
    public static final String GET_ORDERS_FOR_BID = "getOrdersForBid";
    public static final String GET_COMPLETED_ORDERS = "getCompletedOrders";
    public static final String GET_REVISION_ORDERS = "getRevisionOrders";
    public static final String GET_CANCELED_ORDERS = "getCanceledOrders";
    public static final String GET_NEW_ORDERS = "getOrderForBidandAvailableOrders";
    public static final String GET_MY_ORDERS = "getCurrentCompletedCanceledOrders";
    public static final String GET_ORDERS_STATUS_WISE = "getCurrentCompletedOrders";

    public static final String ALL = "All";
    public static final String AVAILABLE = "Available";
    public static final String CURRENT = "Current";
    public static final String FOR_BID = "For Bid";
    public static final String COMPLETED = "Completed";
    public static final String REVISION = "Revision";
    public static final String CANCELLED = "Cancelled";

    public static final String AVAILABLE_ORDER = "AVAILABLE_ORDER";
    public static final String FORBID_ORDER = "FORBID_ORDER";
    public static final String CURRENT_ORDER = "CURRENT_ORDER";
    public static final String COMPLETED_ORDER = "COMPLETED_ORDER";
    public static final String CANCELED_ORDER = "CANCELED_ORDER";

    public static final String UNPAID_PAYMENT_STATUS = "UNPAID_PAYMENT_STATUS";
    public static final String PAID_PAYMENT_STATUS = "PAID_PAYMENT_STATUS";
    public static final String BOTH = "BOTH";

    public static final String YEAR_ONLY_DATE = "YEAR_ONLY_DATE";
    public static final String MONTH_YEAR_DATE = "MONTH_YEAR_DATE";
    public static final String BETWEEN_DATE = "BETWEEN_DATE";
    public static final String ALL_DATE = "ALL";

    public static final String ORDER_TYPE = "OrderType";
    public static final String IS_MY_ORDER = "isMyOrders";

    public static final String ORDER_AWARDED = "Awarded";
    public static final String ORDER_PROCESSING = "Processing";
    public static final String ORDER_WAITING_FOR_PAYMENT = "Waiting for Payment";
    public static final String ORDER_COMPLETED = "Completed";
    public static final String ORDER_CANCELLED = "Cancelled";
}
