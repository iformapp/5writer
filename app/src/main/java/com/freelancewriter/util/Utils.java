package com.freelancewriter.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static SpannableStringBuilder getBoldString(Context context, String s, String[] fonts, int[] colorList, String[] words) {
        if (TextUtils.isEmpty(s))
            return null;

        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        try {
            for (int i = 0; i < words.length; i++) {
                if (s.contains(words[i])) {
                    if (colorList != null) {
                        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, colorList[i])), s.indexOf(words[i]),
                                s.indexOf(words[i]) + words[i].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }

                    if (fonts != null && fonts.length > i) {
                        Typeface font = Typeface.createFromAsset(context.getAssets(), fonts[i]);
                        ss.setSpan (new CustomTypefaceSpan("", font), s.indexOf(words[i]), s.indexOf(words[i]) + words[i].length(),
                                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ss;
    }

    public static SpannableStringBuilder getColorString(Context context, String s, String word, int color) {
        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), s.indexOf(word),
                s.indexOf(word) + word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void openSoftKeyboard(Activity activity, View view) {
        if (activity == null)
            return;

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static String numberFormat(String number) {
        if (TextUtils.isEmpty(number)) {
            return "";
        }
        try {
            Double d = Double.parseDouble(number);
            NumberFormat nf = new DecimalFormat("#.####");
            return nf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static String numberFormat2Places(String number) {
        if (TextUtils.isEmpty(number)) {
            return "";
        }
        try {
            Double d = Double.parseDouble(number);
            NumberFormat nf = new DecimalFormat("0.00");
            return nf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static String doubleDigit(double number) {
        String s = numberFormat(String.valueOf(number));
        if (s.length() == 1) {
            s = "0" + s;
        } else if (s.length() > 3) {
            s = s.substring(0, 2) + "..";
        }
        return s;
    }

    public static String doubleDigit(String number) {
        String s = numberFormat(number);
        if (s.length() == 1) {
            s = "0" + s;
        } else if (s.length() > 3) {
            s = s.substring(0, 2) + "..";
        }
        return s;
    }

    public static String changeDateFormat(String source, String target, String dateString) {
        if (TextUtils.isEmpty(dateString)) {
            return "";
        }
        SimpleDateFormat input = new SimpleDateFormat(source);
        SimpleDateFormat output = new SimpleDateFormat(target);
        try {
            Date date = input.parse(dateString);
            return output.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static String priceWith$(String price) {
        if (TextUtils.isEmpty(price)) {
            return "";
        }
        if (price.contains("$")) {
            price = price.substring(1);
        }
        return "$" + price;
    }

    public static String priceWithout$(String price) {
        if (TextUtils.isEmpty(price)) {
            return "";
        }
        if (price.contains("$")) {
            price = price.substring(1);
        }
        return price;
    }
}
