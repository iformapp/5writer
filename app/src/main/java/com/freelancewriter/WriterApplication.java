package com.freelancewriter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.freelancewriter.api.ApiClient;
import com.freelancewriter.api.ApiInterface;
import com.freelancewriter.model.GeneralModel;
import com.freelancewriter.model.TypesModel;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Preferences;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WriterApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        //TODO :: replace apiKey and appId with live
        Intercom.initialize(this, "android_sdk-409f1cca72c430a48b720749a857a7c31a5da596", "je6f9lsz");

        Registration registration = Registration.create();
        if (Preferences.getUserData(this) != null) {
            registration.withEmail(Preferences.getUserData(this).email);
            registration.withUserId(Preferences.getUserData(this).userId);
            Intercom.client().registerIdentifiedUser(registration);
        } else {
            Intercom.client().registerUnidentifiedUser();
        }

        //testMethod();
        getTypes(Constants.PAPER_TYPES);
        getTypes(Constants.DISCIPLIN_TYPES);
        getTypes(Constants.FORMATED_STYLE_TYPES);
        getTypes(Constants.SUBJECTS_TYPES);
        getTypes(Constants.CATEGORY_TYPES);

        TypesModel typesModel = new TypesModel();
        List<TypesModel.Data> list = new ArrayList<>();
        TypesModel.Data data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.any_writer);
        data.academicLevelId = "any_writer";
        list.add(data);

        data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.top_10_writer);
        data.academicLevelId = "top_10_writer";
        list.add(data);

        data = new TypesModel.Data();
        data.id = "4";
        data.acctId = "2";
        data.academicLevelName = getString(R.string.my_old_writer);
        data.academicLevelId = "my_previous_writer";
        list.add(data);

        typesModel.data = list;

        Preferences.saveTypes(getApplicationContext(), typesModel.data, Constants.ACADEMIC_TYPES);
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void getTypes(final String types) {
        if (!isNetworkConnected())
            return;

        Call<TypesModel> call = getService().getType(types);
        call.enqueue(new Callback<TypesModel>() {
            @Override
            public void onResponse(Call<TypesModel> call, Response<TypesModel> response) {
                TypesModel typesModel = response.body();
                if (typesModel != null) {
                    if (checkStatus(typesModel)) {
                        Log.e("Response ==>  ", types);
                        Preferences.saveTypes(getApplicationContext(), typesModel.data, types);
                    }
                }
            }

            @Override
            public void onFailure(Call<TypesModel> call, Throwable t) {
                //Toast.makeText(WriterApplication.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean checkStatus(GeneralModel model) {
        if (model.success != null) {
            switch (model.success) {
                case "1":
                    return true;
            }
        } else if (model.flag != null) {
            return model.flag.equals("1");
        }
        return false;
    }
}
