package com.freelancewriter.api;

import com.freelancewriter.model.CompletedFiles;
import com.freelancewriter.model.FileUpload;
import com.freelancewriter.model.GeneralModel;
import com.freelancewriter.model.MyFileModel;
import com.freelancewriter.model.OrderByIdModel;
import com.freelancewriter.model.TypesModel;
import com.freelancewriter.util.Constants;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET(Constants.URL)
    Call<TypesModel> getType(@Query("method") String method);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<Object> login(@Field("method") String method, @Field("email") String email,
                       @Field("password") String password, @Field("login") String login,
                       @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<Object> register(@Field("first_name") String first_name, @Field("last_name") String last_name,
                          @Field("email") String email, @Field("password") String password,
                          @Field("mobile") String mobile, @Field("method") String method,
                          @Field("telephone_prefix") String telephone_prefix, @Field("country") String country,
                          @Field("iso_code") String isoCode, @Field("device_id") String deviceId);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> updateProfile(@Field("method") String method, @Field("accesstoken") String accesstoken,
                                     @Field("user_id") String userId, @Field("fullname") String fullname,
                                     @Field("telephone") String telephone, @Field("iso_code") String isoCode);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> logout(@Field("method") String method, @Field("user_id") String userId);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> applyJob(@Field("method") String method, @Field("writer_id") String userId,
                                @Field("order_id") String orderId, @Field("writer_comments") String writerComment,
                                @Field("rating") String rating, @Field("accesstoken") String accesstoken);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> cancelApplication(@Field("method") String method, @Field("user_id") String userId,
                                         @Field("order_id") String orderId, @Field("accesstoken") String accesstoken);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> forgotPassword(@Field("method") String method, @Field("user_email") String email);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> setPassword(@Field("method") String method, @Field("old_password") String old_password,
                                   @Field("user_password") String user_password, @Field("user_id") String user_id,
                                   @Field("accesstoken") String accesstoken);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> resetPassword(@Field("method") String method, @Field("user_password") String userPassword,
                                     @Field("otp") String otp);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<GeneralModel> sendFeedback(@Field("method") String method, @Field("accesstoken") String accesstoken,
                                    @Field("user_id") String userid, @Field("feedback") String feedback);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<Object> getOrder(@Field("method") String method, @Field("user_id") String userId,
                          @Field("accesstoken") String accesstoken);

    @FormUrlEncoded
    @POST(Constants.DELETE_ORDER)
    Call<GeneralModel> deleteOrder(@Field("accesstoken") String accesstoken, @Field("user_id") String userId,
                                   @Field("order_id") String orderId);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<OrderByIdModel> getOrderById(@Field("method") String method, @Field("user_id") String userId,
                                      @Field("order_id") String orderId, @Field("accesstoken") String accesstoken);

    @Multipart
    @POST(Constants.URL)
    Call<MyFileModel> uploadPaper(@Part("method") RequestBody method, @Part MultipartBody.Part[] file,
                                  @Part("file_name") RequestBody file_name, @Part("file_word_no") RequestBody file_word_no,
                                  @Part("order_id") RequestBody order_id, @Part("writer_order_status") RequestBody writer_order_status,
                                  @Part("comment") RequestBody comment, @Part("user_id") RequestBody user_id,
                                  @Part("accesstoken") RequestBody accesstoken);

    @Multipart
    @POST(Constants.URL)
    Call<FileUpload> uploadNewFile(@Part("method") RequestBody method, @Part MultipartBody.Part[] file,
                                   @Part("order_id") RequestBody order_id, @Part("user_id") RequestBody user_id,
                                   @Part("accesstoken") RequestBody accesstoken);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<CompletedFiles> getCompletedFiles(@Field("method") String method, @Field("user_id") String user_id, @Field("accesstoken") String accesstoken,
                                           @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST(Constants.URL)
    Call<Object> getOrderStatusWise(@Field("method") String method, @Field("user_id") String userId,
                                    @Field("accesstoken") String accessToken, @Field("payment_status") String paymentStatus,
                                    @Field("order_type") String orderType, @Field("assigned_date_from") String assignedDateFrom,
                                    @Field("assigned_date_to") String assignedDateTo, @Field("spec_month_year") String specMonthYear,
                                    @Field("date_filter_type") String dateFilterType);
}
