package com.freelancewriter.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.freelancewriter.R;
import com.freelancewriter.model.GeneralModel;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.ui.order.OrderDetailsActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderAdapter extends RecyclerSwipeAdapter<OrderAdapter.SimpleViewHolder> {

    private Context mContext;
    private List<OrderModel.Data> mDataset;
    private EditOrderListner editOrderListner;
    private String filterType;

    public OrderAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_items, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(List<OrderModel.Data> objects, String filterType) {
        this.mDataset = objects;
        this.filterType = filterType;
        notifyDataSetChanged();
    }

    public interface EditOrderListner {
        public void onEditOrder();
    }

    public void setEditOrderListner(EditOrderListner editOrderListner) {
        this.editOrderListner = editOrderListner;
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder viewHolder, final int position) {
        final OrderModel.Data item = mDataset.get(position);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        viewHolder.rlItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, OrderDetailsActivity.class);
                i.putExtra(Constants.ORDER_DATA, item);
                i.putExtra(Constants.FILTER_TYPE, filterType);
                mContext.startActivity(i);
                ((BaseActivity) mContext).openToLeft();
            }
        });

        viewHolder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteDialog(item.orderId, viewHolder.swipeLayout, position);
            }
        });

        viewHolder.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editOrderListner != null) {
                    editOrderListner.onEditOrder();
                }
            }
        });
        viewHolder.tvOrderid.setText(item.orderNumber);
        viewHolder.tvDate.setText(Utils.changeDateFormat("MM-dd-yyyy hh:mm:ss", "MMM d, yyyy", item.orderDate));
        viewHolder.tvPrice.setText("$" + item.total);

        viewHolder.swipeLayout.setSwipeEnabled(false);

        if (item.orderType != null && !TextUtils.isEmpty(item.orderType)) {
            switch (item.orderType) {
                case Constants.AVAILABLE_ORDER:
                    viewHolder.tvAvailableStatus.setVisibility(View.VISIBLE);
                    viewHolder.tvAvailableStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.black_button_bg));
                    viewHolder.tvAvailableStatus.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                    viewHolder.tvAvailableStatus.setText(Constants.AVAILABLE);
                    break;
                case Constants.FORBID_ORDER:
                    viewHolder.tvAvailableStatus.setVisibility(View.VISIBLE);
                    viewHolder.tvAvailableStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.yellow_button_bg));
                    viewHolder.tvAvailableStatus.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                    viewHolder.tvAvailableStatus.setText(Constants.FOR_BID);
                    break;
                default:
                    viewHolder.tvAvailableStatus.setVisibility(View.GONE);
                    break;
            }
        } else {
            viewHolder.tvAvailableStatus.setVisibility(View.GONE);
        }

        if (item.orderStatusName.equalsIgnoreCase(Constants.ORDER_PROCESSING) ||
                item.orderStatusName.equalsIgnoreCase(Constants.ORDER_AWARDED)) {
            viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue));
            String timerText = Utils.doubleDigit(item.dayint) + " days: " + Utils.doubleDigit(item.hoursint) + " h: " +
                    Utils.doubleDigit(item.minutes) + " m: " + Utils.doubleDigit(item.seconds) + " s";
            if (item.isDue != null && item.isDue) {
                viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.waiting_button_bg));
                viewHolder.tvStatus.setText("Late:" + timerText);
            } else {
                viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.processing_button_bg));
                viewHolder.tvStatus.setText(timerText);
            }
        } else if (item.orderStatusName.equalsIgnoreCase(Constants.ORDER_COMPLETED)) {
            viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.completed_button_bg));
            viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green));
            viewHolder.tvStatus.setText(item.orderStatusName);
        } else if (item.orderStatusName.equalsIgnoreCase(Constants.ORDER_CANCELLED)) {
            viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.reset_pw_btn));
            viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.waiting_button_bg));
            viewHolder.tvStatus.setText(item.orderStatusName);
        } else {
            viewHolder.dividerLine.setBackgroundColor(ContextCompat.getColor(mContext, R.color.reset_pw_btn));
            viewHolder.tvStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.waiting_button_bg));
            viewHolder.tvStatus.setText(item.orderStatusName);
        }
        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_itemview)
        RelativeLayout rlItemView;
        @BindView(R.id.ll_edit)
        LinearLayout llEdit;
        @BindView(R.id.ll_delete)
        LinearLayout llDelete;
        @BindView(R.id.divider_line)
        View dividerLine;
        @BindView(R.id.tv_orderid)
        TextView tvOrderid;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.swipe)
        SwipeLayout swipeLayout;
        @BindView(R.id.tv_available_status)
        TextView tvAvailableStatus;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showDeleteDialog(final String orderId, final SwipeLayout swipeLayout, final int position) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = (TextView) dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(Utils.fromHtml(String.format(mContext.getString(R.string.delete_order_text), orderId)));

        tvCancel.setText(mContext.getString(R.string.no));
        tvChatnow.setText(mContext.getString(R.string.yes));
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteOrders(orderId, swipeLayout, position);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void deleteOrders(String orderId, final SwipeLayout swipeLayout, final int position) {
        final BaseActivity activity = (BaseActivity) mContext;
        if (activity == null || !activity.isNetworkConnected())
            return;

        activity.showProgress();

        Call<GeneralModel> call = activity.getService().deleteOrder(activity.getAccessToken(), activity.getUserId(), orderId);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (activity.checkStatus(response.body())) {
                        mItemManger.removeShownLayouts(swipeLayout);
                        mDataset.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, mDataset.size());
                        mItemManger.closeAllItems();
                        Toast.makeText(activity, "Deleted", Toast.LENGTH_SHORT).show();
                    }
                }
                activity.hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                activity.failureError("delete Order failed");
            }
        });
    }
}
