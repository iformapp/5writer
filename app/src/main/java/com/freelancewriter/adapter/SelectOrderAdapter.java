package com.freelancewriter.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.freelancewriter.R;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.textview.TextViewSFTextBold;
import com.freelancewriter.util.textview.TextViewSFTextRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectOrderAdapter extends RecyclerView.Adapter<SelectOrderAdapter.SimpleViewHolder>
        implements Filterable {

    private List<OrderModel.Data> mDataset;
    private List<OrderModel.Data> mDatasetFiltered;
    private Context context;

    public SelectOrderAdapter(Context context, List<OrderModel.Data> objects) {
        this.mDataset = objects;
        this.mDatasetFiltered = objects;
        this.context = context;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_types, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position) {
        OrderModel.Data item = mDatasetFiltered.get(position);
        holder.tvTitle.setText(item.orderNumber);
        holder.tvHeading.setVisibility(View.GONE);

        if (item.isSelected) {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_BOLD);
            holder.tvTitle.setTypeface(tf);
            holder.imgChecked.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));
            Typeface tf = Typeface.createFromAsset(context.getAssets(), Constants.SFTEXT_REGULAR);
            holder.tvTitle.setTypeface(tf);
            holder.imgChecked.setVisibility(View.GONE);
        }

        holder.rlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelected();
                mDatasetFiltered.get(position).isSelected = true;
                notifyDataSetChanged();
            }
        });
    }

    private void clearSelected() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                mDatasetFiltered.get(i).isSelected = false;
            }
        }
    }

    public OrderModel.Data getSelectedItem() {
        if (mDatasetFiltered != null && mDatasetFiltered.size() > 0) {
            for (int i = 0; i < mDatasetFiltered.size(); i++) {
                if (mDatasetFiltered.get(i).isSelected) {
                    return mDatasetFiltered.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered != null ? mDatasetFiltered.size() : 0;
    }

    public List<OrderModel.Data> getData() {
        return mDatasetFiltered;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_heading)
        TextViewSFTextBold tvHeading;
        @BindView(R.id.tv_title)
        TextViewSFTextRegular tvTitle;
        @BindView(R.id.img_checked)
        ImageView imgChecked;
        @BindView(R.id.rl_view)
        View rlView;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDatasetFiltered = mDataset;
                } else {
                    List<OrderModel.Data> filteredList = new ArrayList<>();
                    for (OrderModel.Data row : mDataset) {
                        if (!TextUtils.isEmpty(row.orderNumber)) {
                            if (row.orderNumber.contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (List<OrderModel.Data>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
