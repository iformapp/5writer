package com.freelancewriter.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.freelancewriter.R;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.ui.neworder.UploadPaperActivity;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadFileAdapter extends RecyclerView.Adapter<UploadFileAdapter.SimpleViewHolder> {

    private List<File> mDataset;
    private Context context;
    private BaseActivity activity;

    public UploadFileAdapter(Context context) {
        this.context = context;
        activity = ((BaseActivity) context);
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uploaded_files, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(List<File> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        viewHolder.tvFileName.setText(mDataset.get(position).getName());

        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDataset.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mDataset.size());
                ((UploadPaperActivity) context).setFileAdapter(mDataset);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_file_name)
        TextViewSFDisplayRegular tvFileName;
        @BindView(R.id.img_delete)
        ImageView imgDelete;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
