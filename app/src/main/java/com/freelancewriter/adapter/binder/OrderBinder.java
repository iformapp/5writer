package com.freelancewriter.adapter.binder;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahamed.multiviewadapter.BaseViewHolder;
import com.ahamed.multiviewadapter.ItemBinder;
import com.freelancewriter.R;
import com.freelancewriter.model.OrderDetailsModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderBinder extends ItemBinder<OrderDetailsModel, OrderBinder.OrderViewHolder> {

    private Context mContext;

    @Override
    public OrderViewHolder create(LayoutInflater inflater, ViewGroup parent) {
        mContext = parent.getContext();
        return new OrderViewHolder(inflater.inflate(R.layout.item_order_details, parent, false));
    }

    @Override
    public boolean canBindData(Object item) {
        return item instanceof OrderDetailsModel;
    }

    @Override
    public void bind(OrderViewHolder holder, OrderDetailsModel item) {
        holder.tvDescription.setText(item.description);
        holder.tvTitle.setText(item.title);
        if (!TextUtils.isEmpty(item.price)) {
            holder.tvTitle.setTextColor(Color.BLACK);
            holder.tvYesNo.setVisibility(View.VISIBLE);
            holder.tvPrice.setText(item.price);
            if (item.isSelect) {
                holder.tvYesNo.setText("Yes");
                holder.tvYesNo.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_round_corner));
            } else {
                holder.tvYesNo.setText("No");
                holder.tvYesNo.setBackground(ContextCompat.getDrawable(mContext, R.drawable.red_round_corner));
            }
        } else {
            holder.tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.textColorAccent));
            holder.tvYesNo.setVisibility(View.GONE);
        }
    }

    static class OrderViewHolder extends BaseViewHolder<OrderDetailsModel> {

        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_description)
        TextView tvDescription;
        @BindView(R.id.tv_yes_no)
        TextView tvYesNo;

        public OrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}