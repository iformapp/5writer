package com.freelancewriter.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancewriter.BuildConfig;
import com.freelancewriter.R;
import com.freelancewriter.model.OrderByIdModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.util.MyDownloadManager;
import com.freelancewriter.util.MyDownloadManager.DownloadCompleteListener;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WriterFileAdapter extends RecyclerView.Adapter<WriterFileAdapter.SimpleViewHolder> {

    private List<OrderByIdModel.WriterFiles> mDataset;
    private Context mContext;
    private File file;
    private boolean isDownload = false;
    private boolean isEmailShare = false;
    private boolean isShare = false;

    public WriterFileAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_files, parent, false);
        return new SimpleViewHolder(view);
    }

    public void doRefresh(List<OrderByIdModel.WriterFiles> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        final OrderByIdModel.WriterFiles item = mDataset.get(position);

        holder.tvFileName.setText(item.fileName);
        holder.tvDate.setText(Utils.changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd/MM/yyyy", item.dateTime));
        holder.tvOwner.setText("By Client");
        holder.imgView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.menu_dot));

        holder.imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionDialog(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_file_name)
        TextViewSFDisplayRegular tvFileName;
        @BindView(R.id.tv_date)
        TextViewSFDisplayRegular tvDate;
        @BindView(R.id.tv_owner)
        TextViewSFDisplayRegular tvOwner;
        @BindView(R.id.img_view)
        ImageView imgView;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showOptionDialog(final OrderByIdModel.WriterFiles writerFiles) {
        final Dialog dialog = new Dialog(mContext, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_file_option_menu);
        dialog.setCancelable(true);

        View llView = dialog.findViewById(R.id.ll_view);
        View llDownload = dialog.findViewById(R.id.ll_download);
        View llEmail = dialog.findViewById(R.id.ll_email);
        View llShare = dialog.findViewById(R.id.ll_share);
        TextView btnCancel = dialog.findViewById(R.id.btn_cancel);

        llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isDownload = false;
                isEmailShare = false;
                isShare = false;
                checkPermission(writerFiles, false, false, false);
            }
        });

        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isDownload = true;
                isEmailShare = false;
                isShare = false;
                checkPermission(writerFiles, true, false, false);
            }
        });

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isDownload = false;
                isEmailShare = false;
                isShare = true;
                checkPermission(writerFiles, false, false, true);
            }
        });

        llEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isDownload = false;
                isEmailShare = true;
                isShare = false;
                checkPermission(writerFiles, false, true, false);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void downloadFile(final OrderByIdModel.WriterFiles writerFiles, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        File folder = new File(Environment.getExternalStorageDirectory(), "/Download/" + mContext.getString(R.string.app_name));
        if (!folder.exists())
            folder.mkdir();

        final File file = new File(folder, writerFiles.fileName);
        if (!file.exists()) {
            ((BaseActivity) mContext).showProgress();

            MyDownloadManager downloadManager = new MyDownloadManager(mContext)
                    .setDownloadUrl(writerFiles.filePath)
                    .setTitle(writerFiles.fileName)
                    .setDestinationUri(file)
                    .setDownloadCompleteListener(new DownloadCompleteListener() {
                        @Override
                        public void onDownloadComplete() {
                            ((BaseActivity) mContext).hideProgress();
                            showOutput("Download complete", isDownload, file, isEmailShare, isShare);
                        }

                        @Override
                        public void onDownloadFailure() {
                            ((BaseActivity) mContext).hideProgress();
                            Toast.makeText(mContext, "Download failed", Toast.LENGTH_SHORT).show();
                        }
                    });
            downloadManager.startDownload();
        } else {
            showOutput("Already Downloaded", isDownload, file, isEmailShare, isShare);
        }
    }

    private void showOutput(String message, boolean isDownload, File file, boolean isEmailShare, boolean isShare) {
        if (!isDownload) {
            if (isShare || isEmailShare)
                shareFile(file, isEmailShare);
            else
                viewFile(file);
        } else {
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void viewFile(File file) {
        try {
            Uri uri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String mime = "*/*";
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String extension = mimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (mimeTypeMap.hasExtension(extension))
                mime = mimeTypeMap.getMimeTypeFromExtension(extension);
            intent.setDataAndType(uri, mime);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            mContext.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareFile(File file, boolean isEmailShare) {
        Uri uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider", file);
        Intent intentShareFile = new Intent(isEmailShare ? Intent.ACTION_SENDTO : Intent.ACTION_SEND);
        intentShareFile.setType("*/*");
        if (isEmailShare) {
            intentShareFile.setData(Uri.parse("mailto:"));
            intentShareFile.putExtra(Intent.EXTRA_EMAIL, "");
        }
        intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);
        mContext.startActivity(Intent.createChooser(intentShareFile, "Share File"));
    }

    private void checkPermission(final OrderByIdModel.WriterFiles writerFiles, final boolean isDownload, final boolean isEmailShare, final boolean isShare) {
        Dexter.withActivity((Activity) mContext)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        downloadFile(writerFiles, isDownload, isEmailShare, isShare);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(mContext, "give storage permission first", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
}
