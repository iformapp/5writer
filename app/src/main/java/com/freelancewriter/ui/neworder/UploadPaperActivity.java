package com.freelancewriter.ui.neworder;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancewriter.R;
import com.freelancewriter.adapter.SelectOrderAdapter;
import com.freelancewriter.adapter.UploadFileAdapter;
import com.freelancewriter.model.FileUpload;
import com.freelancewriter.model.MyFileModel;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.ui.MainActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.edittext.EditTextSFTextRegular;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;
import com.freelancewriter.util.textview.TextViewSFTextRegular;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;

public class UploadPaperActivity extends BaseActivity {

    @BindView(R.id.tv_file_name)
    EditTextSFTextRegular tvFileName;
    @BindView(R.id.tv_word_count)
    EditTextSFTextRegular tvWordCount;
    @BindView(R.id.et_details)
    EditTextSFTextRegular etDetails;
    @BindView(R.id.tv_files_count)
    TextViewSFTextRegular tvFilesCount;
    @BindView(R.id.rv_files)
    RecyclerView rvFiles;
    @BindView(R.id.tv_no_files)
    TextViewSFDisplayBold tvNoFiles;
    @BindView(R.id.tv_uploadfilename)
    TextViewSFTextRegular tvUploadfilename;
    @BindView(R.id.ll_files)
    LinearLayout llFiles;

    private int writerOrderId = Constants.COMPLETED_ID;
    private String orderId;
    private String filePath;
    private ArrayList<File> fileList;
    private UploadFileAdapter fileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_papers);
        ButterKnife.bind(this);

        rvFiles.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick({R.id.tv_upload_info, R.id.rl_upload_files, R.id.rl_save_order})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_upload_info:
                showToolTip(view, getString(R.string.tooltip_upload));
                break;
            case R.id.rl_upload_files:
                selectFileDialog();
                break;
            case R.id.rl_save_order:
                if (isValid())
                    uploadPaper();
                break;
        }
    }

    public boolean isValid() {
        if (isEmpty(orderId)) {
            validationError("Please select Order first");
            return false;
        }

        if (isEmpty(getFileName())) {
            validationError("Please enter file name");
            return false;
        }

        if (isEmpty(getWordCount())) {
            validationError("Please enter word count");
            return false;
        }

        if (isEmpty(getComments())) {
            validationError("Please enter comment");
            return false;
        }

        String[] split = getComments().split(" ");
        if (split.length < 3) {
            validationError("Please enter atleast 3 words in comment");
            return false;
        }

        if (llFiles.getVisibility() == View.GONE) {
            validationError("Please select File");
            return false;
        }

        return true;
    }

    private void showAlertDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(getString(R.string.upload_file_msg));

        tvCancel.setText(getString(R.string.cancel));
        tvChatnow.setText(getString(R.string.ok));
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                selectFileDialog();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void selectFileDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_camera_document_select);
        dialog.setCancelable(true);
        TextView tvCancel = dialog.findViewById(R.id.btn_cancel);
        LinearLayout llCamera = dialog.findViewById(R.id.ll_camera);
        LinearLayout llDocument = dialog.findViewById(R.id.ll_document);

        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(false);
                dialog.dismiss();
            }
        });

        llDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(true);
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void checkPermission(final boolean isDocument) {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (isDocument) {
                                Intent intent = new Intent(UploadPaperActivity.this, NormalFilePickActivity.class);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                intent.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
                            } else {
                                Intent intent = new Intent(UploadPaperActivity.this, ImagePickActivity.class);
                                intent.putExtra(IS_NEED_CAMERA, true);
                                intent.putExtra(Constant.MAX_NUMBER, 5);
                                startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            Toast.makeText(UploadPaperActivity.this, "Please give permission", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fileList = new ArrayList<>();
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<NormalFile> docPaths = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    if (docPaths != null && docPaths.size() > 0) {
                        Log.e("Doc Path == > ", docPaths.get(0).getPath());
                        for (NormalFile file : docPaths) {
                            fileList.add(new File(file.getPath()));
                        }
                        setFileAdapter(fileList);
                        //tvUploadfilename.setText(docPaths.get(0).getName());
                    } else {
                        Toast.makeText(this, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ArrayList<ImageFile> imgPath = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    if (imgPath != null && imgPath.size() > 0) {
                        Log.e("Image Path == > ", imgPath.get(0).getPath());
                        for (ImageFile file : imgPath) {
                            fileList.add(new File(file.getPath()));
                        }
                        setFileAdapter(fileList);
                        //tvUploadfilename.setText(imgPath.get(0).getName());
                    } else {
                        Toast.makeText(this, "File not selected", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    public void uploadPaper() {
        if (!isNetworkConnected())
            return;

        showProgress();

        MultipartBody.Part[] body = null;
        if (fileList != null && fileList.size() > 0) {
            body = new MultipartBody.Part[fileList.size()];
            for (int i = 0; i < fileList.size(); i++) {
                File file = fileList.get(i);
                Uri selectedUri = Uri.fromFile(file);
                String fileExtension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());

                RequestBody requestFile = null;
                if (mimeType != null) {
                    requestFile = RequestBody.create(MediaType.parse(mimeType), file);
                }

                if (requestFile != null) {
                    body[i] = MultipartBody.Part.createFormData("file_docs[]", file.getName(), requestFile);
                }
            }
        }

        RequestBody method = RequestBody.create(MultipartBody.FORM, Constants.UPLOAD_PAPER);
        RequestBody filename = RequestBody.create(MultipartBody.FORM, getFileName());
        RequestBody filewordno = RequestBody.create(MultipartBody.FORM, getWordCount());
        RequestBody comment = RequestBody.create(MultipartBody.FORM, getComments());
        RequestBody writerorderstatus = RequestBody.create(MultipartBody.FORM, String.valueOf(writerOrderId));
        RequestBody order_id = RequestBody.create(MultipartBody.FORM, orderId);
        RequestBody user_id = RequestBody.create(MultipartBody.FORM, getUserId());
        RequestBody access_token = RequestBody.create(MultipartBody.FORM, getAccessToken());

        Call<MyFileModel> call = getService().uploadPaper(method, body, filename, filewordno, order_id,
                writerorderstatus, comment, user_id, access_token);
        call.enqueue(new Callback<MyFileModel>() {
            @Override
            public void onResponse(Call<MyFileModel> call, Response<MyFileModel> response) {
                MyFileModel fileUpload = response.body();
                if (checkStatus(fileUpload)) {
                    tvFileName.setText("");
                    tvWordCount.setText("");
                    etDetails.setText("");
                    tvUploadfilename.setText(getString(R.string.upload_files_photo));
                    tvFilesCount.setText("0 Files");
                    filePath = "";
                    llFiles.setVisibility(View.GONE);
                    Toast.makeText(UploadPaperActivity.this, fileUpload.msg, Toast.LENGTH_SHORT).show();
                }

                hideProgress();
            }

            @Override
            public void onFailure(Call<MyFileModel> call, Throwable t) {
                failureError("file upload failed");
            }
        });
    }

    public void setFileAdapter(List<File> uploadedfiles) {
        if (uploadedfiles != null && uploadedfiles.size() > 0) {
            tvFilesCount.setText(uploadedfiles.size() + " Files");

            if (fileAdapter == null) {
                fileAdapter = new UploadFileAdapter(UploadPaperActivity.this);
            }

            fileAdapter.doRefresh(uploadedfiles);

            if (rvFiles.getAdapter() == null) {
                rvFiles.setAdapter(fileAdapter);
            }
            llFiles.setVisibility(View.VISIBLE);
        } else {
            llFiles.setVisibility(View.GONE);
            tvFilesCount.setText("0 Files");
        }
    }

    public String getFileName() {
        return tvFileName.getText().toString().trim();
    }

    public String getWordCount() {
        return tvWordCount.getText().toString().trim();
    }

    public String getComments() {
        return etDetails.getText().toString().trim();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }
}
