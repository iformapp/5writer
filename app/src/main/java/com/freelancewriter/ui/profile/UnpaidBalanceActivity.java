package com.freelancewriter.ui.profile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.util.Attributes;
import com.freelancewriter.R;
import com.freelancewriter.adapter.UnpaidOrdersAdapter;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.freelancewriter.util.textview.TextViewSFTextBold;
import com.freelancewriter.util.textview.TextViewSFTextRegular;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UnpaidBalanceActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tv_payment_status)
    TextViewSFTextRegular tvPaymentStatus;
    @BindView(R.id.tv_date_range)
    TextViewSFTextRegular tvDateRange;
    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_no_orders)
    TextViewSFDisplayBold tvNoOrders;
    @BindView(R.id.tv_balance)
    TextViewSFTextBold tvBalance;
    @BindView(R.id.tv_balance_text)
    TextViewSFTextRegular tvBalanceText;
    @BindView(R.id.ll_balance)
    LinearLayout llBalance;

    private List<OrderModel.Data> orderArrayList;
    private UnpaidOrdersAdapter mAdapter;
    private boolean isPullToRefresh = false;
    private Dialog dialog;
    private TextView tvUnpaid;
    private TextView tvPaid;
    private TextView tvPaymentBoth;
    private TextView tvCompleted;
    private TextView tvCurrent;
    private TextView tvOrderBoth;
    private TextView tvCancel;
    private TextView tvApply;
    private TextView tvFirstDate;
    private TextView tvLastDate;
    private TextView tvMonth;
    private TextView tvYear;
    private TextView tvAll;
    private TextView tvStartDate;
    private TextView tvEndDate;
    private String paymentStatus = Constants.UNPAID_PAYMENT_STATUS;
    private String orderType = Constants.COMPLETED_ORDER;
    private String dateType = "";
    private String startDate;
    private String endDate;
    private String specMothYear = "";
    private boolean isSpecificDate = false;
    private boolean isFirstRangeDate = true;
    private String currentMonth, firstRangeDate, secRangeDate;
    private int currentYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unpaid_balance);
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        orderArrayList = new ArrayList<>();
        rvOrders.setLayoutManager(new LinearLayoutManager(this));

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM");
        currentMonth = dateFormat.format(calendar.getTime());
        int monthMaxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        currentYear = calendar.get(Calendar.YEAR);
        firstRangeDate = currentMonth + " 1, " + currentYear + " - " + currentMonth + " 15, " + currentYear;
        secRangeDate = currentMonth + " 16, " + currentYear + " - " + currentMonth + " " + monthMaxDays + ", " + currentYear;

        tvDateRange.setText(firstRangeDate);
        startDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", currentMonth + " 1, " + currentYear);
        endDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", currentMonth + " 15, " + currentYear);
        dateType = Constants.BETWEEN_DATE;

        getOrders();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isPullToRefresh = true;
                getOrders();
            }
        });
    }

    public void getOrders() {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }
            return;
        }

        if (!isPullToRefresh)
            showProgress();

        Call<Object> orderCall = getService().getOrderStatusWise(Constants.GET_ORDERS_STATUS_WISE, getUserId(),
                getAccessToken(), paymentStatus, orderType, startDate, endDate, specMothYear, dateType);
        orderCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                try {
                    isPullToRefresh = false;
                    orderArrayList = new ArrayList<>();
                    String jsonResposne = new Gson().toJson(response.body());
                    if (checkStatus(jsonResposne)) {
                        Log.e("Orders Response", jsonResposne);
                        OrderModel orderModel = getModel(jsonResposne, OrderModel.class);
                        orderArrayList = orderModel.data;

                        switch (paymentStatus) {
                            case Constants.UNPAID_PAYMENT_STATUS:
                                tvBalanceText.setText("Unpaid Balance:");
                                tvBalance.setText("$" + orderModel.balances.unpaidBalance);
                                llBalance.setBackground(ContextCompat.getDrawable(UnpaidBalanceActivity.this, R.drawable.red_button_bg_20));
                                break;
                            case Constants.PAID_PAYMENT_STATUS:
                                tvBalanceText.setText("Paid Balance:");
                                tvBalance.setText("$" + orderModel.balances.paidBalance);
                                llBalance.setBackground(ContextCompat.getDrawable(UnpaidBalanceActivity.this, R.drawable.green_button_bg));
                                break;
                            case Constants.BOTH:
                                tvBalanceText.setText("Total Balance:");
                                tvBalance.setText("$" + orderModel.balances.totalBalance);
                                llBalance.setBackground(ContextCompat.getDrawable(UnpaidBalanceActivity.this, R.drawable.green_button_bg));
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setAdapter();
                hideProgress();
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("Order failed");
                isPullToRefresh = false;
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public void setAdapter() {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            tvNoOrders.setVisibility(View.GONE);
            llBalance.setVisibility(View.VISIBLE);
            if (mAdapter == null) {
                mAdapter = new UnpaidOrdersAdapter(this);
                mAdapter.setMode(Attributes.Mode.Single);
            }

            mAdapter.doRefresh(orderArrayList);

            if (rvOrders != null && rvOrders.getAdapter() == null) {
                rvOrders.setAdapter(mAdapter);
            }
        } else {
            if (mAdapter != null) {
                mAdapter.doRefresh(null);
            }
            llBalance.setVisibility(View.GONE);
            tvNoOrders.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No orders found", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.ll_back, R.id.tv_payment_status, R.id.tv_date_range})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_payment_status:
                showPaymentStatusDialog();
                break;
            case R.id.tv_date_range:
                showDateRangeDialog();
                break;
        }
    }

    public void showPaymentStatusDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_payment_status);
        dialog.setCancelable(true);

        tvUnpaid = dialog.findViewById(R.id.tv_unpaid);
        tvPaid = dialog.findViewById(R.id.tv_paid);
        tvPaymentBoth = dialog.findViewById(R.id.tv_payment_both);
        tvCompleted = dialog.findViewById(R.id.tv_completed);
        tvCurrent = dialog.findViewById(R.id.tv_current);
        tvOrderBoth = dialog.findViewById(R.id.tv_order_both);
        tvCancel = dialog.findViewById(R.id.tv_cancel);
        tvApply = dialog.findViewById(R.id.tv_apply);

        tvUnpaid.setOnClickListener(this);
        tvPaid.setOnClickListener(this);
        tvPaymentBoth.setOnClickListener(this);
        tvCompleted.setOnClickListener(this);
        tvCurrent.setOnClickListener(this);
        tvOrderBoth.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvApply.setOnClickListener(this);

        if (!isEmpty(paymentStatus)) {
            clearPaymentViews();
            switch (paymentStatus) {
                case Constants.UNPAID_PAYMENT_STATUS:
                    setSelectedView(tvUnpaid);
                    break;
                case Constants.PAID_PAYMENT_STATUS:
                    setSelectedView(tvPaid);
                    break;
                case Constants.BOTH:
                    setSelectedView(tvPaymentBoth);
                    break;
            }
        } else {
            paymentStatus = Constants.UNPAID_PAYMENT_STATUS;
        }

        if (!isEmpty(orderType)) {
            clearOrdersViews();
            switch (orderType) {
                case Constants.COMPLETED_ORDER:
                    setSelectedView(tvCompleted);
                    break;
                case Constants.CURRENT_ORDER:
                    setSelectedView(tvCurrent);
                    break;
                case Constants.BOTH:
                    setSelectedView(tvOrderBoth);
                    break;
            }
        } else {
            orderType = Constants.COMPLETED_ORDER;
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void showDateRangeDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_date_range);
        dialog.setCancelable(true);

        tvFirstDate = dialog.findViewById(R.id.tv_first_date);
        tvLastDate = dialog.findViewById(R.id.tv_last_date);
        tvMonth = dialog.findViewById(R.id.tv_month);
        tvYear = dialog.findViewById(R.id.tv_year);
        tvAll = dialog.findViewById(R.id.tv_all);
        tvStartDate = dialog.findViewById(R.id.tv_start_date);
        tvEndDate = dialog.findViewById(R.id.tv_end_date);
        tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvApply = dialog.findViewById(R.id.tv_apply);

        if (!isEmpty(dateType)) {
            clearDateViews();
            switch (dateType) {
                case Constants.MONTH_YEAR_DATE:
                    setSelectedView(tvMonth);
                    tvFirstDate.setText(firstRangeDate);
                    tvLastDate.setText(secRangeDate);
                    tvStartDate.setText(currentMonth + " 1, " + currentYear);
                    tvEndDate.setText(currentMonth + " 15, " + currentYear);
                    String[] monthYear = specMothYear.split("\\.");
                    tvMonth.setText(Utils.changeDateFormat("MM", "MMM", monthYear[1]));
                    tvYear.setText(monthYear[0]);
                    break;
                case Constants.YEAR_ONLY_DATE:
                    setSelectedView(tvYear);
                    tvFirstDate.setText(firstRangeDate);
                    tvLastDate.setText(secRangeDate);
                    tvStartDate.setText(currentMonth + " 1, " + currentYear);
                    tvEndDate.setText(currentMonth + " 15, " + currentYear);
                    tvMonth.setText(currentMonth);
                    tvYear.setText(specMothYear);
                    break;
                case Constants.BETWEEN_DATE:
                    if (isSpecificDate) {
                        setSelectedView(tvStartDate);
                        setSelectedView(tvEndDate);
                        tvFirstDate.setText(firstRangeDate);
                        tvLastDate.setText(secRangeDate);
                        tvStartDate.setText(Utils.changeDateFormat("yyyy/MM/dd", "MMM d, yyyy", startDate));
                        tvEndDate.setText(Utils.changeDateFormat("yyyy/MM/dd", "MMM d, yyyy", endDate));
                    } else {
                        String rangeDate = Utils.changeDateFormat("yyyy/MM/dd", "MMM d, yyyy", startDate) + " - " +
                                Utils.changeDateFormat("yyyy/MM/dd", "MMM d, yyyy", endDate);
                        if (isFirstRangeDate) {
                            setSelectedView(tvFirstDate);
                            tvFirstDate.setText(rangeDate);
                            tvLastDate.setText(secRangeDate);
                        } else {
                            setSelectedView(tvLastDate);
                            tvFirstDate.setText(firstRangeDate);
                            tvLastDate.setText(rangeDate);
                        }
                        tvStartDate.setText(currentMonth + " 1, " + currentYear);
                        tvEndDate.setText(currentMonth + " 15, " + currentYear);
                    }
                    tvMonth.setText(currentMonth);
                    tvYear.setText(currentYear + "");
                    break;
                case Constants.ALL_DATE:
                    setSelectedView(tvAll);
                    tvFirstDate.setText(firstRangeDate);
                    tvLastDate.setText(secRangeDate);
                    tvMonth.setText(currentMonth);
                    tvYear.setText(currentYear + "");
                    tvStartDate.setText(currentMonth + " 1, " + currentYear);
                    tvEndDate.setText(currentMonth + " 15, " + currentYear);
                    break;
            }
        } else {
            isFirstRangeDate = true;
            tvFirstDate.setText(firstRangeDate);
            tvLastDate.setText(secRangeDate);
            tvMonth.setText(currentMonth);
            tvYear.setText(currentYear + "");
            tvStartDate.setText(currentMonth + " 1, " + currentYear);
            tvEndDate.setText(currentMonth + " 15, " + currentYear);
        }

        tvFirstDate.setOnClickListener(this);
        tvLastDate.setOnClickListener(this);
        tvMonth.setOnClickListener(this);
        tvYear.setOnClickListener(this);
        tvAll.setOnClickListener(this);
        tvStartDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (dateType) {
                    case Constants.MONTH_YEAR_DATE:
                        tvDateRange.setText(tvMonth.getText().toString());
                        break;
                    case Constants.YEAR_ONLY_DATE:
                        tvDateRange.setText(tvYear.getText().toString());
                        break;
                    case Constants.BETWEEN_DATE:
                        if (isSpecificDate) {
                            tvDateRange.setText(tvStartDate.getText().toString() + " - " + tvEndDate.getText().toString());
                        } else {
                            if (isFirstRangeDate) {
                                tvDateRange.setText(tvFirstDate.getText().toString());
                            } else {
                                tvDateRange.setText(tvLastDate.getText().toString());
                            }
                        }
                        break;
                    case Constants.ALL_DATE:
                        tvDateRange.setText(tvAll.getText().toString());
                        break;
                }
                getOrders();
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    private void selectFromDate() {
        int year, month, day;
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
                String date = dateFormat.format(calendar.getTime());
                tvStartDate.setText(date);
                clearDateViews();
                setSelectedView(tvStartDate);
                setSelectedView(tvEndDate);
                dateType = Constants.BETWEEN_DATE;
                isSpecificDate = true;
                isFirstRangeDate = false;
                startDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", tvStartDate.getText().toString());
                specMothYear = "";
            }
        }, year, month, day);

        datePickerDialog.show();
        //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    private void selectToDate(String fromDate) {
        if (TextUtils.isEmpty(fromDate)) {
            validationError("Select Start Date First");
            return;
        }

        int year = 0, month = 0, day = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        Date fDate;
        try {
            fDate = dateFormat.parse(fromDate);
            month = Integer.parseInt((String) DateFormat.format("MM", fDate)) - 1;
            year = Integer.parseInt((String) DateFormat.format("yyyy", fDate));
            day = Integer.parseInt((String) DateFormat.format("dd", fDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.set(year, month, day);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
                tvEndDate.setText(dateFormat.format(calendar.getTime()));
                clearDateViews();
                setSelectedView(tvStartDate);
                setSelectedView(tvEndDate);
                dateType = Constants.BETWEEN_DATE;
                isSpecificDate = true;
                isFirstRangeDate = false;
                endDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", tvEndDate.getText().toString());
                specMothYear = "";
            }
        }, year, month, day);

        datePickerDialog.show();
        //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
    }

    public void clearOrdersViews() {
        setInvalidateViews(tvCompleted, tvCurrent, tvOrderBoth);
    }

    public void clearDateViews() {
        setInvalidateViews(tvFirstDate, tvLastDate, tvMonth, tvYear, tvAll, tvStartDate, tvEndDate);
    }

    public void clearPaymentViews() {
        setInvalidateViews(tvUnpaid, tvPaid, tvPaymentBoth);
    }

    public void setSelectedView(View v) {
        v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_selected_bg));
        ((TextView) v).setTextColor(Color.WHITE);
    }

    public void setInvalidateViews(View... views) {
        for (View v : views) {
            v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_unselect_bg));
            ((TextView) v).setTextColor(Color.BLACK);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_unpaid:
                clearPaymentViews();
                setSelectedView(tvUnpaid);
                paymentStatus = Constants.UNPAID_PAYMENT_STATUS;
                tvPaymentStatus.setText(getString(R.string.unpaid));
                break;
            case R.id.tv_paid:
                clearPaymentViews();
                setSelectedView(tvPaid);
                paymentStatus = Constants.PAID_PAYMENT_STATUS;
                tvPaymentStatus.setText(getString(R.string.paid));
                break;
            case R.id.tv_payment_both:
                clearPaymentViews();
                setSelectedView(tvPaymentBoth);
                paymentStatus = Constants.BOTH;
                tvPaymentStatus.setText(getString(R.string.both));
                break;
            case R.id.tv_completed:
                clearOrdersViews();
                setSelectedView(tvCompleted);
                orderType = Constants.COMPLETED_ORDER;
                break;
            case R.id.tv_current:
                clearOrdersViews();
                setSelectedView(tvCurrent);
                orderType = Constants.CURRENT_ORDER;
                break;
            case R.id.tv_order_both:
                clearOrdersViews();
                setSelectedView(tvOrderBoth);
                orderType = Constants.BOTH;
                break;
            case R.id.tv_cancel:
                dialog.dismiss();
                break;
            case R.id.tv_apply:
                dialog.dismiss();
                getOrders();
                break;
            case R.id.tv_first_date:
                clearDateViews();
                setSelectedView(tvFirstDate);
                dateType = Constants.BETWEEN_DATE;
                isSpecificDate = false;
                isFirstRangeDate = true;
                String[] getDate = tvFirstDate.getText().toString().split("-");
                startDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", getDate[0].trim());
                endDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", getDate[1].trim());
                specMothYear = "";
                break;
            case R.id.tv_last_date:
                clearDateViews();
                setSelectedView(tvLastDate);
                dateType = Constants.BETWEEN_DATE;
                isSpecificDate = false;
                isFirstRangeDate = false;
                getDate = tvLastDate.getText().toString().split("-");
                startDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", getDate[0].trim());
                endDate = Utils.changeDateFormat("MMM d, yyyy", "yyyy/MM/dd", getDate[1].trim());
                specMothYear = "";
                break;
            case R.id.tv_month:
                clearDateViews();
                setSelectedView(tvMonth);
                isSpecificDate = false;
                isFirstRangeDate = false;
                dateType = Constants.MONTH_YEAR_DATE;
                String month = Utils.changeDateFormat("MMM", "MM", tvMonth.getText().toString());
                String year = tvYear.getText().toString();
                specMothYear = year + "." + month;
                startDate = "";
                endDate = "";
                break;
            case R.id.tv_year:
                clearDateViews();
                setSelectedView(tvYear);
                isSpecificDate = false;
                isFirstRangeDate = false;
                dateType = Constants.YEAR_ONLY_DATE;
                specMothYear = tvYear.getText().toString();
                startDate = "";
                endDate = "";
                break;
            case R.id.tv_all:
                clearDateViews();
                setSelectedView(tvAll);
                isSpecificDate = false;
                isFirstRangeDate = false;
                dateType = Constants.ALL_DATE;
                startDate = "";
                endDate = "";
                specMothYear = "";
                break;
            case R.id.tv_start_date:
                selectFromDate();
                break;
            case R.id.tv_end_date:
                selectToDate(tvStartDate.getText().toString());
                break;
        }
    }
}
