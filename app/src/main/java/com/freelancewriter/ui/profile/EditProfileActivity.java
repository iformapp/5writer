package com.freelancewriter.ui.profile;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.freelancewriter.R;
import com.freelancewriter.ccp.CountryCodePicker;
import com.freelancewriter.model.GeneralModel;
import com.freelancewriter.model.UserModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Preferences;
import com.freelancewriter.util.edittext.EditTextSFTextRegular;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.freelancewriter.util.textview.TextViewSFTextBold;
import com.freelancewriter.util.textview.TextViewSFTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFDisplayBold tvToolbarTitle;
    @BindView(R.id.btn_right)
    TextViewSFTextBold btnRight;
    @BindView(R.id.et_mobile)
    EditTextSFTextRegular etMobile;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.tv_phone_prefix)
    TextViewSFTextRegular tvPhonePrefix;
    @BindView(R.id.tv_email)
    TextViewSFTextRegular tvEmail;
    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;

    private UserModel.Data userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.edit_profile));

        userData = Preferences.getUserData(this);
        if (userData == null) {
            finish();
            return;
        }

        etFirstname.setText(userData.firstName + " " + userData.lastName);
        etLastname.setText(userData.lastName);

        if (userData.email != null) {
            tvEmail.setText(userData.email);
        }

        if (userData.telephone != null) {
            etMobile.setText(userData.telephone);
            if (userData.dialcode != null && !isEmpty(userData.dialcode)) {
                String code = userData.dialcode.replace("+", "");
                tvPhonePrefix.setText("+" + code);
                if (isEmpty(userData.isoCode)) {
                    int countryCode = Integer.parseInt(code);
                    ccp.setCountryForPhoneCode(countryCode);
                } else {
                    ccp.setCountryForNameCode(userData.isoCode);
                }
            }
        }

        addTextChangeEvent(etFirstname, etLastname, etMobile);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                btnRight.setVisibility(View.VISIBLE);
                tvPhonePrefix.setText(ccp.getSelectedCountryCodeWithPlus());
            }
        });
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getFirstName() {
        return etFirstname.getText().toString().trim();
    }

    public String getLastName() {
        return etLastname.getText().toString().trim();
    }

    public String getMobile() {
        return etMobile.getText().toString().trim();
    }

    public String getMobilePrefix() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    public String getISOCode() {
        return ccp.getSelectedCountryNameCode();
    }

    public void updateProfile() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().updateProfile(Constants.UPDATE_PROFILE, getAccessToken(), getUserId(), getFirstName(),
                getMobile(), getISOCode());
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (checkStatus(response.body())) {
                    String[] fullName = getFirstName().split(" ");
                    if (fullName.length > 0) {
                        if (!isEmpty(fullName[0])) {
                            userData.firstName = fullName[0];
                        } else {
                            userData.firstName = "";
                        }

                        if (fullName.length > 1 && !isEmpty(fullName[1])) {
                            userData.lastName = fullName[1];
                        } else {
                            userData.lastName = "";
                        }
                    }

                    userData.telephone = getMobile();
                    userData.dialcode = getMobilePrefix();
                    userData.isoCode = getISOCode();

                    Preferences.saveUserData(EditProfileActivity.this, userData);
                    Toast.makeText(EditProfileActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                    btnRight.setVisibility(View.GONE);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update profile failed");
            }
        });
    }

    public void addTextChangeEvent(EditText... editTexts) {
        for (EditText edittext : editTexts) {
            edittext.addTextChangedListener(textWatcher);
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (btnRight.getVisibility() == View.GONE) {
                btnRight.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @OnClick({R.id.ll_back, R.id.btn_right, R.id.tv_changepassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.btn_right:
                if (validData()) {
                    updateProfile();
                }
                break;
            case R.id.tv_changepassword:
                redirectActivity(UpdatePasswordActivity.class);
                break;
        }
    }

    public boolean validData() {
        if (isEmpty(getFirstName())) {
            validationError("Enter First Name");
            return false;
        }

//        if (isEmpty(getLastName())) {
//            validationError("Enter Last Name");
//            return false;
//        }

        if (isEmpty(getMobile())) {
            validationError("Enter Mobile Number");
            return false;
        }

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);
        try {
            Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(getMobile(), getISOCode().toUpperCase());
            if (!phoneUtil.isValidNumber(swissNumberProto)) {
                validationError("Enter Valid Mobile no");
                return false;
            }
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            validationError("Enter Valid Mobile no");
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToRight();
    }
}
