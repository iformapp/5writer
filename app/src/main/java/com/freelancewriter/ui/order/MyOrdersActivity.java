package com.freelancewriter.ui.order;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.freelancewriter.R;
import com.freelancewriter.fragment.OrdersFragment;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyOrdersActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.tv_order)
    TextViewSFDisplayBold tvOrder;

    private String[] tabTexts;
    private TextView tvAll;
    private TextView tvCurrent;
    private TextView tvForBid;
    private TextView tvCompleted;
    private TextView tvRevision;
    private TextView tvCanceled;
    private TextView tvCancel;
    private TextView tvApply;
    private Dialog dialog;
    private String filterType = Constants.GET_AVAILABLE_ORDERS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_orders);
        ButterKnife.bind(this);

        tvOrder.setText(getString(R.string.my_orders));

        tabTexts = new String[]{Constants.ALL, Constants.COMPLETED, Constants.CURRENT};
        setupTabs();
    }

    public void setupTabs() {
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_BOLD);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_REGULAR);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(ContextCompat.getColor(MyOrdersActivity.this, R.color.textColorAccent));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabText();
    }

    private void setupTabText() {
        for (int i = 0; i < tabTexts.length; i++) {
            TextView tv = (TextView) LayoutInflater.from(this)
                    .inflate(i == 0 ? R.layout.custom_tab_text_select : R.layout.custom_tab_text_unselect, null);
            tv.setText(tabTexts[i]);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        viewPager.setOffscreenPageLimit(tabLayout.getTabCount() - 1);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (String tabText : tabTexts) {
            adapter.addFrag(OrdersFragment.newInstanace(tabText, true), tabText);
        }
        viewPager.setAdapter(adapter);
    }

    @OnClick(R.id.img_filter)
    public void onViewClicked() {
        showFilterDialog();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    public void showFilterDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_filter);
        dialog.setCancelable(true);

        tvAll = dialog.findViewById(R.id.tv_available);
        tvCurrent = dialog.findViewById(R.id.tv_current);
        tvForBid = dialog.findViewById(R.id.tv_for_bid);
        tvCompleted = dialog.findViewById(R.id.tv_completed);
        tvRevision = dialog.findViewById(R.id.tv_revision);
        tvCanceled = dialog.findViewById(R.id.tv_canceled);
        tvCancel = dialog.findViewById(R.id.tv_cancel);
        tvApply = dialog.findViewById(R.id.tv_apply);

        tvAll.setText("All");
        tvAll.setOnClickListener(this);
        tvCurrent.setOnClickListener(this);
        tvForBid.setOnClickListener(this);
        tvCompleted.setOnClickListener(this);
        tvRevision.setOnClickListener(this);
        tvCanceled.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvApply.setOnClickListener(this);

        clearViews();
        switch (filterType) {
            case Constants.GET_AVAILABLE_ORDERS:
                setSelectedView(tvAll);
                break;
            case Constants.GET_CURRENT_ORDERS:
                setSelectedView(tvCurrent);
                break;
            case Constants.GET_ORDERS_FOR_BID:
                setSelectedView(tvForBid);
                break;
            case Constants.GET_COMPLETED_ORDERS:
                setSelectedView(tvCompleted);
                break;
            case Constants.GET_REVISION_ORDERS:
                setSelectedView(tvRevision);
                break;
            case Constants.GET_CANCELED_ORDERS:
                setSelectedView(tvCanceled);
                break;
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_available:
                clearViews();
                setSelectedView(tvAll);
                filterType = Constants.GET_AVAILABLE_ORDERS;
                break;
            case R.id.tv_current:
                clearViews();
                setSelectedView(tvCurrent);
                filterType = Constants.GET_CURRENT_ORDERS;
                break;
            case R.id.tv_for_bid:
                clearViews();
                setSelectedView(tvForBid);
                filterType = Constants.GET_ORDERS_FOR_BID;
                break;
            case R.id.tv_completed:
                clearViews();
                setSelectedView(tvCompleted);
                filterType = Constants.GET_COMPLETED_ORDERS;
                break;
            case R.id.tv_revision:
                clearViews();
                setSelectedView(tvRevision);
                filterType = Constants.GET_REVISION_ORDERS;
                break;
            case R.id.tv_canceled:
                clearViews();
                setSelectedView(tvCanceled);
                filterType = Constants.GET_CANCELED_ORDERS;
                break;
            case R.id.tv_cancel:
                dialog.dismiss();
                break;
            case R.id.tv_apply:
                if (filterType.equals(Constants.GET_AVAILABLE_ORDERS)) {
                    imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter));
                } else {
                    imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter_selected));
                }
                dialog.dismiss();
                //getOrders(filterType);
                break;
        }
    }

    public void clearViews() {
        setInvalidateViews(tvAll, tvCompleted, tvCurrent, tvRevision, tvForBid, tvCanceled);
    }

    public void setSelectedView(View v) {
        v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_selected_bg));
        ((TextView) v).setTextColor(Color.WHITE);
    }

    public void setInvalidateViews(View... views) {
        for (View v : views) {
            v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_unselect_bg));
            ((TextView) v).setTextColor(Color.BLACK);
        }
    }
}
