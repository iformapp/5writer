package com.freelancewriter.ui.order;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.util.Attributes;
import com.freelancewriter.R;
import com.freelancewriter.adapter.OrderAdapter;
import com.freelancewriter.adapter.OrderAdapter.EditOrderListner;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.ui.MainActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends BaseActivity implements View.OnClickListener, EditOrderListner {

    @BindView(R.id.tvTitle)
    TextViewSFDisplayBold tvTitle;
    @BindView(R.id.tvInfo)
    TextViewSFDisplayRegular tvInfo;
    @BindView(R.id.img_delete)
    ImageView imgDelete;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.rl_without_login)
    RelativeLayout rlWithoutLogin;
    @BindView(R.id.ll_order_view)
    RelativeLayout llOrderView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_order_title)
    TextViewSFDisplayBold tvOrderTitle;
    @BindView(R.id.tv_view_order)
    TextViewSFDisplayRegular tvViewOrder;
    @BindView(R.id.tv_no_orders)
    TextViewSFDisplayBold tvNoOrders;

    private List<OrderModel.Data> orderArrayList;
    private OrderAdapter mAdapter;
    private boolean isDeleteSelected = false;
    private boolean isFilterSelected = false;
    private Dialog dialog;
    private TextView tvAvailable;
    private TextView tvCurrent;
    private TextView tvForBid;
    private TextView tvCompleted;
    private TextView tvRevision;
    private TextView tvCanceled;
    private TextView tvCancel;
    private TextView tvApply;
    private String filterType = Constants.GET_AVAILABLE_ORDERS;
    private boolean isPullToRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
    }

    public void init() {
        if (imgFilter != null) {
            imgFilter.setVisibility(View.VISIBLE);
        }
        filterType = Constants.GET_AVAILABLE_ORDERS;
        tvOrderTitle.setText(getString(R.string.available) + " " + getString(R.string.orders));
        tvViewOrder.setText(getString(R.string.view_all_your_orders, getString(R.string.available)));
        imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter));
        orderArrayList = new ArrayList<>();
        rvOrders.setLayoutManager(new LinearLayoutManager(this));
        getOrders(filterType);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isPullToRefresh = true;
                getOrders(filterType);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLogin()) {
            llOrderView.setVisibility(View.VISIBLE);
            rlWithoutLogin.setVisibility(View.GONE);

            if (orderArrayList == null || orderArrayList.isEmpty()) {
                init();
            } else {
                getOrders(filterType);
            }
        } else {
            tvTitle.setText(getString(R.string.my_orders));
            tvInfo.setText(getString(R.string.login_to_orders));
            llOrderView.setVisibility(View.GONE);
            rlWithoutLogin.setVisibility(View.VISIBLE);
        }
    }

    public void getOrders(String method) {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }
            return;
        }

        if (!isPullToRefresh)
            showProgress();

        Call<Object> orderCall = getService().getOrder(method, getUserId(), getAccessToken());
        orderCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                try {
                    isPullToRefresh = false;
                    orderArrayList = new ArrayList<>();
                    String jsonResposne = new Gson().toJson(response.body());
                    if (checkStatus(jsonResposne)) {
                        Log.e("Orders Response", jsonResposne);
                        OrderModel orderModel = getModel(jsonResposne, OrderModel.class);
                        orderArrayList = orderModel.data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setAdapter();
                hideProgress();
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("Order failed");
                isPullToRefresh = false;
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public void setAdapter() {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            if (imgDelete != null) {
//                switch (filterType) {
//                    case Constants.GET_AVAILABLE_ORDERS:
//                    case Constants.GET_ORDERS_FOR_BID:
//                        imgDelete.setVisibility(View.VISIBLE);
//                        break;
//                    default:
//                        imgDelete.setVisibility(View.GONE);
//                        break;
//                }
                imgDelete.setVisibility(View.GONE);
            }
            tvNoOrders.setVisibility(View.GONE);
            if (mAdapter == null) {
                mAdapter = new OrderAdapter(this);
                mAdapter.setEditOrderListner(this);
                mAdapter.setMode(Attributes.Mode.Single);
            }

            mAdapter.doRefresh(orderArrayList, filterType);

            if (rvOrders != null && rvOrders.getAdapter() == null) {
                rvOrders.setAdapter(mAdapter);
            }
        } else {
            if (imgDelete != null) {
                imgDelete.setVisibility(View.GONE);
            }
            if (mAdapter != null) {
                mAdapter.doRefresh(null, filterType);
            }
            tvNoOrders.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No orders found", Toast.LENGTH_SHORT).show();
        }
    }

    public void showFilterDialog() {
        dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_filter);
        dialog.setCancelable(true);

        tvAvailable = dialog.findViewById(R.id.tv_available);
        tvCurrent = dialog.findViewById(R.id.tv_current);
        tvForBid = dialog.findViewById(R.id.tv_for_bid);
        tvCompleted = dialog.findViewById(R.id.tv_completed);
        tvRevision = dialog.findViewById(R.id.tv_revision);
        tvCanceled = dialog.findViewById(R.id.tv_canceled);
        tvCancel = dialog.findViewById(R.id.tv_cancel);
        tvApply = dialog.findViewById(R.id.tv_apply);

        tvAvailable.setOnClickListener(this);
        tvCurrent.setOnClickListener(this);
        tvForBid.setOnClickListener(this);
        tvCompleted.setOnClickListener(this);
        tvRevision.setOnClickListener(this);
        tvCanceled.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvApply.setOnClickListener(this);

        clearViews();
        switch (filterType) {
            case Constants.GET_AVAILABLE_ORDERS:
                setSelectedView(tvAvailable);
                break;
            case Constants.GET_CURRENT_ORDERS:
                setSelectedView(tvCurrent);
                break;
            case Constants.GET_ORDERS_FOR_BID:
                setSelectedView(tvForBid);
                break;
            case Constants.GET_COMPLETED_ORDERS:
                setSelectedView(tvCompleted);
                break;
            case Constants.GET_REVISION_ORDERS:
                setSelectedView(tvRevision);
                break;
            case Constants.GET_CANCELED_ORDERS:
                setSelectedView(tvCanceled);
                break;
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_available:
                clearViews();
                setSelectedView(tvAvailable);
                filterType = Constants.GET_AVAILABLE_ORDERS;
                break;
            case R.id.tv_current:
                clearViews();
                setSelectedView(tvCurrent);
                filterType = Constants.GET_CURRENT_ORDERS;
                break;
            case R.id.tv_for_bid:
                clearViews();
                setSelectedView(tvForBid);
                filterType = Constants.GET_ORDERS_FOR_BID;
                break;
            case R.id.tv_completed:
                clearViews();
                setSelectedView(tvCompleted);
                filterType = Constants.GET_COMPLETED_ORDERS;
                break;
            case R.id.tv_revision:
                clearViews();
                setSelectedView(tvRevision);
                filterType = Constants.GET_REVISION_ORDERS;
                break;
            case R.id.tv_canceled:
                clearViews();
                setSelectedView(tvCanceled);
                filterType = Constants.GET_CANCELED_ORDERS;
                break;
            case R.id.tv_cancel:
                dialog.dismiss();
                break;
            case R.id.tv_apply:
                switch (filterType) {
                    case Constants.GET_AVAILABLE_ORDERS:
                        tvOrderTitle.setText(getString(R.string.available) + " " + getString(R.string.orders));
                        tvViewOrder.setText(getString(R.string.view_all_your_orders, getString(R.string.available)));
                        break;
                    case Constants.GET_CURRENT_ORDERS:
                        tvOrderTitle.setText(getString(R.string.current) + " " + getString(R.string.orders));
                        tvViewOrder.setText(getString(R.string.view_all_your_orders, getString(R.string.current)));
                        break;
                    case Constants.GET_ORDERS_FOR_BID:
                        tvOrderTitle.setText(getString(R.string.for_bid) + " " + getString(R.string.orders));
                        tvViewOrder.setText(getString(R.string.view_all_your_orders, getString(R.string.for_bid)));
                        break;
                    case Constants.GET_COMPLETED_ORDERS:
                        tvOrderTitle.setText(getString(R.string.completed) + " " + getString(R.string.orders));
                        tvViewOrder.setText(getString(R.string.view_all_your_orders, getString(R.string.completed)));
                        break;
                    case Constants.GET_REVISION_ORDERS:
                        tvOrderTitle.setText(getString(R.string.revision) + " " + getString(R.string.orders));
                        tvViewOrder.setText(getString(R.string.view_all_your_orders, getString(R.string.revision)));
                        break;
                    case Constants.GET_CANCELED_ORDERS:
                        tvOrderTitle.setText(getString(R.string.canceled) + " " + getString(R.string.orders));
                        tvViewOrder.setText(getString(R.string.view_all_your_orders, getString(R.string.canceled)));
                        break;
                }

                if (filterType.equals(Constants.GET_AVAILABLE_ORDERS)) {
                    imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter));
                } else {
                    imgFilter.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter_selected));
                }
                dialog.dismiss();
                getOrders(filterType);
                break;
        }
    }

    public void clearViews() {
        setInvalidateViews(tvAvailable, tvCompleted, tvCurrent, tvRevision, tvForBid, tvCanceled);
    }

    public void setSelectedView(View v) {
        v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_selected_bg));
        ((TextView) v).setTextColor(Color.WHITE);
    }

    public void setInvalidateViews(View... views) {
        for (View v : views) {
            v.setBackground(ContextCompat.getDrawable(this, R.drawable.filter_unselect_bg));
            ((TextView) v).setTextColor(Color.BLACK);
        }
    }

    @OnClick({R.id.btn_signup, R.id.btn_login, R.id.img_delete, R.id.img_filter})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(false);
                } else {
                    goToLoginSignup(false, true);
                }
                break;
            case R.id.btn_login:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(true);
                } else {
                    goToLoginSignup(true, true);
                }
                break;
            case R.id.img_delete:
                setToggleDelete(isDeleteSelected);
                break;
            case R.id.img_filter:
                showFilterDialog();
                break;
        }
    }

    public void setToggleDelete(boolean isDeleteSelected) {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            if (isDeleteSelected) {
                if (mAdapter != null)
                    mAdapter.mItemManger.closeItem(0);
            } else {
                if (mAdapter != null)
                    mAdapter.mItemManger.openItem(0);
            }
            imgDelete.setImageDrawable(ContextCompat.getDrawable(this,
                    isDeleteSelected ? R.drawable.delete : R.drawable.delete_selected));
            this.isDeleteSelected = !isDeleteSelected;
        }
    }

    public void setToggleDeleteOff() {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            imgDelete.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.delete));
            isDeleteSelected = false;
        }
    }

    @Override
    public void onEditOrder() {
        showEditDialog();
    }

    public void showEditDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_Dialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_edit_order);
        dialog.setCancelable(true);

        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvChatnow = (TextView) dialog.findViewById(R.id.tv_chat_now);

        tvMessage.setText(Utils.fromHtml(getString(R.string.edit_order_text)));

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvChatnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                redirectTab(Constants.TAB_CHAT);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.CENTER;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }
}
