package com.freelancewriter.ui.order;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freelancewriter.R;
import com.freelancewriter.fragment.OrderApplyFragment;
import com.freelancewriter.fragment.OrderDetailsFragment;
import com.freelancewriter.fragment.OrderFilesFragment;
import com.freelancewriter.fragment.OrderStatusFragment;
import com.freelancewriter.fragment.UploadPaperFragment;
import com.freelancewriter.model.OrderByIdModel;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.freelancewriter.util.textview.TextViewSFTextBold;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tv_order_no)
    TextViewSFDisplayBold tvOrderNo;
    @BindView(R.id.tv_no_order)
    TextViewSFTextBold tvNoOrder;
    @BindView(R.id.ll_tabs)
    LinearLayout llTabs;

    private OrderByIdModel.Data orderData;
    private OrderModel.Data singleData;
    private boolean isRefresh = false;
    public String filterType;
    public String orderType;

    private String[] tabTexts;
    private boolean isCancelledOrder = false;
    private boolean isAvailableForBidOrder = false;
    private boolean isCurrentCompleteOrder = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            singleData = (OrderModel.Data) getIntent().getSerializableExtra(Constants.ORDER_DATA);
            filterType = getIntent().getStringExtra(Constants.FILTER_TYPE);
            orderType = singleData.orderType;
        }
        if (singleData == null)
            return;

        if (orderType != null && !isEmpty(orderType)) {
            switch (orderType) {
                case Constants.AVAILABLE_ORDER:
                    isAvailableForBidOrder = true;
                    tabTexts = new String[]{"Status", "Details", "Files", "Apply"};
                    break;
                case Constants.FORBID_ORDER:
                    isAvailableForBidOrder = true;
                    tabTexts = new String[]{"Status", "Details", "Files", "Cancel"};
                    break;
                case Constants.COMPLETED_ORDER:
                case Constants.CURRENT_ORDER:
                    isCurrentCompleteOrder = true;
                    tabTexts = new String[]{"Status", "Details", "Files", "Upload"};
                    break;
                case Constants.CANCELED_ORDER:
                    isCancelledOrder = true;
                    tabTexts = new String[]{"Status"};
                    tabLayout.setVisibility(View.GONE);
                    break;
                default:
                    tabTexts = new String[]{"Details", "Status", "Files"};
                    break;
            }
        } else {
            switch (filterType) {
                case Constants.GET_AVAILABLE_ORDERS:
                    isAvailableForBidOrder = true;
                    tabTexts = new String[]{"Status", "Details", "Files", "Apply"};
                    break;
                case Constants.GET_ORDERS_FOR_BID:
                    isAvailableForBidOrder = true;
                    tabTexts = new String[]{"Status", "Details", "Files", "Cancel"};
                    break;
                case Constants.GET_COMPLETED_ORDERS:
                case Constants.GET_CURRENT_ORDERS:
                    isCurrentCompleteOrder = true;
                    tabTexts = new String[]{"Status", "Details", "Files", "Upload"};
                    break;
                case Constants.GET_CANCELED_ORDERS:
                    isCancelledOrder = true;
                    tabTexts = new String[]{"Status"};
                    tabLayout.setVisibility(View.GONE);
                    break;
                default:
                    tabTexts = new String[]{"Details", "Status", "Files"};
                    break;
            }
        }
        tvOrderNo.setText(singleData.orderNumber);
        getOrderById(singleData.orderId);
    }

    public void cancelOrderUi() {
        tabTexts = new String[]{"Status"};
        tabLayout.setVisibility(View.GONE);
        getOrderById(singleData.orderId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getOrderById(singleData.orderId);
        }
    }

    public void setupTabs() {
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_BOLD);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                Typeface tf = Typeface.createFromAsset(getAssets(), Constants.SFDISPLAY_REGULAR);
                if (text != null) {
                    text.setTypeface(tf);
                    text.setTextColor(ContextCompat.getColor(OrderDetailsActivity.this, R.color.textColorAccent));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabText();

        if (tabLayout.getVisibility() == View.VISIBLE)
            llTabs.setVisibility(View.VISIBLE);
    }

    public void getOrderById(String orderId) {
        if (!isNetworkConnected()) {
            return;
        }

        showProgress();

        Call<OrderByIdModel> call = getService().getOrderById(Constants.ORDER_DETAIL, getUserId(), orderId, getAccessToken());
        call.enqueue(new Callback<OrderByIdModel>() {
            @Override
            public void onResponse(Call<OrderByIdModel> call, Response<OrderByIdModel> response) {
                OrderByIdModel orderModel = response.body();
                if (checkStatus(orderModel)) {
                    orderData = orderModel.data;
                    setupTabs();
                } else {
                    isRefresh = true;
                    viewPager.setVisibility(View.GONE);
                    tvNoOrder.setVisibility(View.VISIBLE);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<OrderByIdModel> call, Throwable t) {
                failureError(getString(R.string.order_details_not_found));
                viewPager.setVisibility(View.GONE);
                tvNoOrder.setVisibility(View.VISIBLE);
            }
        });
    }

    public boolean isAvailableOrders() {
        return isAvailableForBidOrder;
    }

    public String orderType() {
        return singleData.orderType;
    }

    public String getFilterType() {
        return filterType;
    }

    public OrderByIdModel.Data getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderByIdModel.Data orderData) {
        this.orderData = orderData;
    }

    private void setupTabText() {
        for (int i = 0; i < tabTexts.length; i++) {
            TextView tv = (TextView) LayoutInflater.from(this)
                    .inflate(i == 0 ? R.layout.custom_tab_text_select : R.layout.custom_tab_text_unselect, null);
            tv.setText(tabTexts[i]);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        viewPager.setOffscreenPageLimit(tabLayout.getTabCount() - 1);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(OrderStatusFragment.newInstanace(singleData), getString(R.string.status));
        if (!isCancelledOrder) {
            adapter.addFrag(OrderDetailsFragment.newInstanace(false), getString(R.string.details));
            adapter.addFrag(OrderFilesFragment.newInstanace(false), getString(R.string.files));
            if (isAvailableForBidOrder) {
                adapter.addFrag(OrderApplyFragment.newInstanace(singleData), getString(R.string.apply));
            }
            if (isCurrentCompleteOrder) {
                adapter.addFrag(UploadPaperFragment.newInstanace(singleData), getString(R.string.upload));
            }
        }
        viewPager.setAdapter(adapter);
    }

    @OnClick({R.id.ll_back, R.id.tv_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_chat:
                Intercom.client().displayMessenger();
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }
}
