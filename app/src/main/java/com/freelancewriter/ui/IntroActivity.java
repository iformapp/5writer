package com.freelancewriter.ui;

import android.os.Bundle;
import android.view.View;

import com.freelancewriter.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntroActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isLogin()) {
            redirectActivity(MainActivity.class);
            finish();
            return;
        }
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_become_writer, R.id.btn_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_become_writer:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(false);
                } else {
                    goToLoginSignup(false, false);
                }
                break;
            case R.id.btn_login:
                if (getParent() != null) {
                    ((MainActivity) getParent()).goToLoginSignup(true);
                } else {
                    goToLoginSignup(true, false);
                }
                break;
        }
    }
}
