package com.freelancewriter.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancewriter.R;
import com.freelancewriter.ccp.CountryCodePicker;
import com.freelancewriter.model.GeneralModel;
import com.freelancewriter.model.UserModel;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.Preferences;
import com.freelancewriter.util.Utils;
import com.freelancewriter.util.edittext.EditTextSFTextRegular;
import com.freelancewriter.util.textview.TextViewSFDisplayRegular;
import com.freelancewriter.util.textview.TextViewSFTextRegular;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSignUpActivity extends BaseActivity {

    private static final int LOGIN = 0;
    private static final int SIGNUP = 1;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.tab_login)
    SegmentedButton tabLogin;
    @BindView(R.id.tab_signup)
    SegmentedButton tabSignup;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.et_password)
    EditTextSFTextRegular etPassword;
    @BindView(R.id.ll_login)
    LinearLayout llLogin;
    @BindView(R.id.ll_signup)
    LinearLayout llSignup;
    @BindView(R.id.ll_back)
    LinearLayout llBack;
    @BindView(R.id.segmentLoginGroup)
    SegmentedButtonGroup segmentLoginGroup;
    @BindView(R.id.et_s_email)
    EditTextSFTextRegular etSEmail;
    @BindView(R.id.et_phone)
    EditTextSFTextRegular etPhone;
    @BindView(R.id.et_s_password)
    EditTextSFTextRegular etSPassword;
    @BindView(R.id.tv_right)
    TextViewSFDisplayRegular tvRight;
    @BindView(R.id.et_firstname)
    EditTextSFTextRegular etFirstname;
    @BindView(R.id.et_lastname)
    EditTextSFTextRegular etLastname;
    @BindView(R.id.tv_phone_prefix)
    TextViewSFTextRegular tvPhonePrefix;

    private boolean isFromLogin = false;
    private boolean isFinish = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            isFromLogin = getIntent().getBooleanExtra(Constants.FROM_LOGIN, true);
            isFinish = getIntent().getBooleanExtra(Constants.IS_FINISH, true);
        }

        if (isFromLogin) {
            llLogin.setVisibility(View.VISIBLE);
            llSignup.setVisibility(View.GONE);
        } else {
            llLogin.setVisibility(View.GONE);
            llSignup.setVisibility(View.VISIBLE);
        }

        tvRight.setText(getString(R.string.chat));

        segmentLoginGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int position) {
                if (position == LOGIN) {
                    isFromLogin = true;
                    tabLogin.setTypeface(Constants.SFTEXT_BOLD);
                    tabSignup.setTypeface(Constants.SFTEXT_REGULAR);
                    llLogin.setVisibility(View.VISIBLE);
                    llSignup.setVisibility(View.GONE);
                } else if (position == SIGNUP) {
                    isFromLogin = false;
                    tabLogin.setTypeface(Constants.SFTEXT_REGULAR);
                    tabSignup.setTypeface(Constants.SFTEXT_BOLD);
                    llLogin.setVisibility(View.GONE);
                    llSignup.setVisibility(View.VISIBLE);
                }
            }
        });

        segmentLoginGroup.setPosition(isFromLogin ? 0 : 1);

        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isWhitespace(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };
        etPassword.setFilters(new InputFilter[]{filter});
        etSPassword.setFilters(new InputFilter[]{filter});

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                tvPhonePrefix.setText(ccp.getSelectedCountryCodeWithPlus());
            }
        });
    }

    public String getEmail() {
        return isFromLogin ? etEmail.getText().toString().trim() : etSEmail.getText().toString().trim();
    }

    public String getPassword() {
        return isFromLogin ? etPassword.getText().toString().trim() : etSPassword.getText().toString().trim();
    }

    public String getFirstName() {
        return etFirstname.getText().toString().trim();
    }

    public String getLastName() {
        return etLastname.getText().toString().trim();
    }

    private String getName() {
        return getLastName() + "," + getFirstName();
    }

    public String getPhone() {
        return etPhone.getText().toString().trim();
    }

    public String getCountryName() {
        return ccp.getSelectedCountryName();
    }

    public String getISOCode() {
        return ccp.getSelectedCountryNameCode();
    }

    public String getCountryCode() {
        return ccp.getSelectedCountryCodeWithPlus();
    }

    public void login() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Log.e("Login Url = > ", Constants.MAIN_URL);
        Log.e("Params", "method : " + Constants.LOGIN + ",  email : " + getEmail() + ", password : " + getPassword() + ", login : " + "Sign In");

        Call<Object> call = getService().login(Constants.LOGIN, getEmail(), getPassword(), "Sign In", getToken());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String jsonResposne = new Gson().toJson(response.body());
                Log.e("Login Response", jsonResposne);
                if (checkStatus(jsonResposne)) {
                    UserModel userModel = getModel(jsonResposne, UserModel.class);
                    if (userModel != null && userModel.data != null) {
                        afterLoginOrSignup(userModel.data);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("login failed");
            }
        });
    }

    public void register() {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<Object> call = getService().register(getFirstName(), getLastName(), getEmail(), getPassword(),
                getPhone(), Constants.REGISTER, getCountryCode(), getCountryName(), getISOCode(), getToken());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String jsonResposne = new Gson().toJson(response.body());
                Log.e("Signup Response", jsonResposne);
                hideProgress();
                if (checkStatus(jsonResposne)) {
                    UserModel userModel = getModel(jsonResposne, UserModel.class);
                    if (userModel != null && userModel.data != null) {
                        //afterLoginOrSignup(userModel.data);
                        login();
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("register failed");
            }
        });
    }

    public void afterLoginOrSignup(UserModel.Data data) {
        Preferences.writeBoolean(LoginSignUpActivity.this, Constants.IS_LOGIN, true);
        Preferences.saveUserData(LoginSignUpActivity.this, data);

        Registration registration = Registration.create();
        registration.withEmail(data.email);
        registration.withUserId(data.userId);
        Intercom.client().registerIdentifiedUser(registration);

        if (isFinish) {
            finish();
        } else {
            gotoMainActivity(Constants.TAB_HOME);
        }
    }

    @OnClick({R.id.ll_back, R.id.btn_login, R.id.btn_signup, R.id.tv_forgot_password, R.id.tv_right})
    public void onViewClicked(View view) {
        Utils.hideSoftKeyboard(this);
        switch (view.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.tv_forgot_password:
                showForgotPasswordDialog();
                break;
            case R.id.tv_right:
                Intercom.client().displayMessenger();
                break;
            case R.id.btn_login:
                if (validLoginData()) {
                    login();
                }
                break;
            case R.id.btn_signup:
                if (validSignUpData()) {
                    register();
                }
                break;
        }
    }

    public boolean validLoginData() {
        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public boolean validSignUpData() {
        if (isEmpty(getFirstName())) {
            validationError("Enter First Name");
            return false;
        }

        if (isEmpty(getLastName())) {
            validationError("Enter Last Name");
            return false;
        }

        if (!isValidEmail(getEmail())) {
            validationError("Enter Valid Email");
            return false;
        }

        if (isEmpty(getPhone())) {
            validationError("Enter Mobile no");
            return false;
        }

        if (isEmpty(getPassword())) {
            validationError("Enter Password");
            return false;
        }

        return true;
    }

    public void showForgotPasswordDialog() {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etEmail = dialog.findViewById(R.id.et_email);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEmail(etEmail.getText().toString())) {
                    Utils.hideSoftKeyboard(LoginSignUpActivity.this);
                    forgotPassword(etEmail.getText().toString(), false);
                    dialog.dismiss();
                } else {
                    Toast.makeText(LoginSignUpActivity.this, "Enter valid email", Toast.LENGTH_SHORT).show();
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void forgotPassword(final String email, final boolean isResend) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().forgotPassword(Constants.FORGOT_PASSWORD, email);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(LoginSignUpActivity.this, response.body().msg, Toast.LENGTH_LONG).show();
                        if (!isResend)
                            showSecurityCodeDialog(email);
                    } else {
                        Toast.makeText(LoginSignUpActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("forgot password failed");
            }
        });
    }

    public void showSecurityCodeDialog(final String email) {
        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_security_code);
        dialog.setCancelable(true);
        Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        Button btnReset = dialog.findViewById(R.id.btn_reset);
        final EditText etSecurityCode = dialog.findViewById(R.id.et_security_code);
        final EditText etNewPassword = dialog.findViewById(R.id.et_new_password);
        TextView tvResendCode = dialog.findViewById(R.id.tv_resend_code);
        tvResendCode.setPaintFlags(tvResendCode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword(email, true);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(etSecurityCode.getText().toString())) {
                    Toast.makeText(LoginSignUpActivity.this, "Please enter code", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isEmpty(etNewPassword.getText().toString())) {
                    Toast.makeText(LoginSignUpActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                    return;
                }

                Utils.hideSoftKeyboard(LoginSignUpActivity.this);
                resetPassword(etSecurityCode.getText().toString(), etNewPassword.getText().toString(), dialog);
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void resetPassword(String otp, String password, final Dialog dialog) {
        if (!isNetworkConnected())
            return;

        showProgress();

        Call<GeneralModel> call = getService().resetPassword(Constants.SET_PASSWORD, password, otp);
        call.enqueue(new Callback<GeneralModel>() {
            @Override
            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                if (response.body() != null) {
                    if (checkStatus(response.body())) {
                        Toast.makeText(LoginSignUpActivity.this, response.body().msg, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } else {
                        failureError(response.body().msg);
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<GeneralModel> call, Throwable t) {
                failureError("update password failed");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishToBottom();
    }
}
