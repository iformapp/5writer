package com.freelancewriter.ui.home;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.daimajia.swipe.util.Attributes;
import com.freelancewriter.R;
import com.freelancewriter.adapter.OrderAdapter;
import com.freelancewriter.model.OrderModel;
import com.freelancewriter.ui.BaseActivity;
import com.freelancewriter.util.Constants;
import com.freelancewriter.util.textview.TextViewSFDisplayBold;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.rv_orders)
    RecyclerView rvOrders;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_no_orders)
    TextViewSFDisplayBold tvNoOrders;

    private boolean isPullToRefresh = false;
    private List<OrderModel.Data> orderArrayList;
    private OrderAdapter mAdapter;
    private String filterType = Constants.GET_CURRENT_ORDERS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        orderArrayList = new ArrayList<>();
        rvOrders.setLayoutManager(new LinearLayoutManager(this));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isPullToRefresh = true;
                getOrders(filterType);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (orderArrayList == null || orderArrayList.isEmpty()) {
            getOrders(filterType);
        }
    }

    public void getOrders(String method) {
        if (!isNetworkConnected()) {
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }
            return;
        }

        if (!isPullToRefresh)
            showProgress();

        Call<Object> orderCall = getService().getOrder(method, getUserId(), getAccessToken());
        orderCall.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                try {
                    isPullToRefresh = false;
                    orderArrayList = new ArrayList<>();
                    String jsonResposne = new Gson().toJson(response.body());
                    if (checkStatus(jsonResposne)) {
                        Log.e("Orders Response", jsonResposne);
                        OrderModel orderModel = getModel(jsonResposne, OrderModel.class);
                        orderArrayList = orderModel.data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setAdapter();
                hideProgress();
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                failureError("Order failed");
                isPullToRefresh = false;
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public void setAdapter() {
        if (orderArrayList != null && orderArrayList.size() > 0) {
            tvNoOrders.setVisibility(View.GONE);
            if (mAdapter == null) {
                mAdapter = new OrderAdapter(this);
                mAdapter.setMode(Attributes.Mode.Single);
            }

            mAdapter.doRefresh(orderArrayList, filterType);

            if (rvOrders != null && rvOrders.getAdapter() == null) {
                rvOrders.setAdapter(mAdapter);
            }
        } else {
            if (mAdapter != null) {
                mAdapter.doRefresh(null, filterType);
            }
            tvNoOrders.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No orders found", Toast.LENGTH_SHORT).show();
        }
    }
}
